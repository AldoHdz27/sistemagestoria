<?php
include_once "config/config.php";

$tipo = 1;
$vin = $_POST['vin'];
$numero_cliente = $_POST['numero_cliente'];
$folio = $_POST['folio'];
$nombre = $_POST['nombre'];
$fecha_1 = $_POST['fecha_1'];
$fecha_2 = $_POST['fecha_2'];
$proceso = $_POST['proceso'];
$estatus = $_POST['estatus'];
$concatenacion = 0;

//Parte de vin
if (!empty($vin) and empty($numero_cliente) and empty($folio) and empty($nombre) and empty($proceso) and empty($estatus) ) {

    if (!empty($fecha_1) and !empty($fecha_2)) {
        $tipo = 1;
        $sp = $con->query("CALL customer($tipo,'$vin','$fecha_1','$fecha_2')");
        $sp_data = $sp->fetch_all();
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($sp_data);

    } else {
        $tipo = 1;
        $sp = $con->query("CALL customer($tipo,'$vin','1000-01-01','9999-12-31')");
        $sp_data = $sp->fetch_all();
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($sp_data);
    }
    
    //Parte Numero de Cliente
} elseif (empty($vin) and !empty($numero_cliente) and empty($folio) and empty($nombre) and empty($proceso) and empty($estatus)) {

    if (!empty($fecha_1) and !empty($fecha_2)) {
        $tipo = 2;
        $sp = $con->query("CALL customer($tipo,'$numero_cliente','$fecha_1','$fecha_2')");
        $sp_data = $sp->fetch_all();
        echo json_encode($sp_data);
    } else {
        $tipo = 2;
        $sp = $con->query("CALL customer($tipo,'$numero_cliente','1000-01-01','9999-12-31')");
        $sp_data = $sp->fetch_all();
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($sp_data);
    }
    
    //Para Folio
} elseif (empty($vin) and empty($numero_cliente) and !empty($folio) and empty($nombre) and empty($proceso) and empty($estatus)) {

    if (!empty($fecha_1) and !empty($fecha_2)) {
        $tipo = 3;
        $sp = $con->query("CALL customer($tipo,'$folio','$fecha_1','$fecha_2')");
        $sp_data = $sp->fetch_all();
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($sp_data);
    } else {
        $tipo = 3;
        $sp = $con->query("CALL customer($tipo,'$folio','1000-01-01','9999-12-31')");
        $sp_data = $sp->fetch_all();
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($sp_data);
    }
    
    //Para nombre
} elseif (empty($vin) and empty($numero_cliente) and empty($folio) and !empty($nombre) and empty($proceso) and empty($estatus) and !empty($fecha_1) and !empty($fecha_2)) {
    $tipo = 4;
    $sp = $con->query("CALL customer($tipo,'$nombre','$fecha_1','$fecha_2')");
    $sp_data = $sp->fetch_all();
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($sp_data);
    
    //Para Proceso
} elseif (empty($vin) and empty($numero_cliente) and empty($folio) and empty($nombre) and !empty($proceso) and (empty($estatus) && $estatus != '0') and !empty($fecha_1) and !empty($fecha_2)) {
    
    $tipo = 5;
    $sp = $con->query("CALL customer($tipo,'$proceso','$fecha_1','$fecha_2')");
    $sp_data = $sp->fetch_all();
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($sp_data);
    

    //Para Estatus
} elseif (empty($vin) and empty($numero_cliente) and empty($folio) and empty($nombre) and empty($proceso) and (!empty($estatus) || $estatus == '0') and !empty($fecha_1) and !empty($fecha_2)) {

    $tipo = 6;
    $sp = $con->query("CALL customer($tipo,'0','$fecha_1','$fecha_2')");
    $sp_data = $sp->fetch_all();

    header('Content-type: application/json; charset=utf-8');
    echo json_encode($sp_data);

    //Para Proceso y Status
} elseif (empty($vin) and empty($numero_cliente) and empty($folio) and empty($nombre) and !empty($proceso) and (!empty($estatus) || $estatus == '0') and !empty($fecha_1) and !empty($fecha_2)) {
    $concatenacion = $proceso . $estatus;
    $tipo = 7;

    $sp = $con->query("CALL customer($tipo,'$concatenacion','$fecha_1','$fecha_2')");
    $sp_data = $sp->fetch_all();

    header('Content-type: application/json; charset=utf-8');
    echo json_encode($sp_data);
    
}
