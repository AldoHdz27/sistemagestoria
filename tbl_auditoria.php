<link rel="icon" href="images/JJ.ico">

<?php 
   $active="active"; 
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 
?>




<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js" ></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>  


<div class="content-wrapper"><!-- Content Wrapper. Contains page content -->
        <section class="content-header"><!-- Content Header (Page header) -->
            <h1></h1>
            <ol class="breadcrumb" hidden>
                <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active"><a href="myfiles.php"><i class="fa fa-upload"></i> Importar Activaciones</a></li>
             </ol>
        </section>


<?php

include "dbconect.php";
$datos = $con->query("select * from tbl_auditoria");
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<h1>Tabla Auditor&iacute;a</h1>
<br><br>

<?php if($datos->num_rows>0):?>
	
	<div class="container-fluid" >
	    <div class="row ">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
    	        <table border="1"id="myTable" class="table table-bordered table-hover nowrap" style="width:100%" >
        	       <thead>
        		<th style="text-align:center">N°Contrato	</th>
                    <th style="text-align:center">Nombre del cliente</th>
                    <th style="text-align:center">vin</th>
                    <th style="text-align:center">Color</th>
                    <th style="text-align:center">Factura</th>
                    
                   <th <?php if($usuario==1){echo "hidden";} ?> style="text-align:center">Doc1</th>
                   <th <?php if($usuario==1){echo "hidden";} ?> style="text-align:center">Doc2</th>
                   <th <?php if($usuario==1){echo "hidden";} ?> style="text-align:center">Doc3</th>
                   <th <?php if($usuario==1){echo "hidden";} ?> style="text-align:center">Doc4</th>
                    
                    
        	       </thead>
        	       <tbody>
            	<?php while($d= $datos->fetch_object()):?>
            		<tr align="center">
                		<td><?php echo $d->n_contrato; ?></td>
                		<td><?php echo $d->nombre_cliente; ?></td>
                		<td><?php echo $d->vin; ?></td>
                		<td><?php echo $d->color; ?></td>
                        <td><?php echo $d->factura; ?></td>
                        
                        <td <?php if($usuario==1){echo "hidden";} ?>>
                            <?php  echo '<form action="upload.php" method="post" enctype="multipart/form-data" >
                                    	<div>
                            		<label for="file-upload" class="custom-file-upload">
                    	    			<i class="fa fa-cloud-upload"></i> Subir Archivo
                    			</label>
                    		        <input  id="file-upload" type="file" name="uploaded_file" />
                            		</div>
                            		<button type="submit"><i class="fa fa-check"></i></button>
                            		
                            		</form>
                            '; ?>
                        
                        </td>
                    
                        <td <?php if($usuario==1){echo "hidden";} ?>>
                            <?php  echo '
                    		<form action="upload.php" method="post" enctype="multipart/form-data" >
                            	<div>
                            		<label for="file-upload" class="custom-file-upload">
                    	    			<i class="fa fa-cloud-upload"></i> Subir Archivo
                    			</label>
                    		        <input  id="file-upload" type="file" name="uploaded_file" />
                            		</div>
                            		<button type="submit"><i class="fa fa-check"></i></button>
                            		
                            		</form>
                    	       '; ?>
                        
                        </td>
                    
                        <td <?php if($usuario==1){echo "hidden";} ?>>
                            <?php  echo '<form action="upload.php" method="post" enctype="multipart/form-data" > 
                            	<div>
                            		<label for="file-upload" class="custom-file-upload">
                    	    			<i class="fa fa-cloud-upload"></i> Subir Archivo
                    			</label>
                    		        <input  id="file-upload" type="file" name="uploaded_file" />
                            		</div>
                            		<button type="submit"><i class="fa fa-check"></i></button>
                            		
                            		</form>
                    	       '; ?>
                   
                        </td>
                    
                        <td <?php if($usuario==1){echo "hidden";} ?>>
                            <?php  echo '<form action="upload.php" method="post" enctype="multipart/form-data" > 
                            	<div>
                            		<label for="file-upload" class="custom-file-upload">
                    	    			<i class="fa fa-cloud-upload"></i> Subir Archivo
                    			</label>
                    		        <input  id="file-upload" type="file" name="uploaded_file" />
                            		</div>
                            		<button type="submit"><i class="fa fa-check"></i></button>
                            		
                            		</form>
                    	   '; ?>
                        
                        </td>        
                    </tr>
        	

        	   <?php endwhile; ?>
    	       
                   </tbody>
                </table>
            </div>
        </div>
    </div>

<?php else:?>
	<h3>No hay Datos</h3>
<?php endif; ?>

</body>



<script>

$(document).ready( function () {
    
    $('#myTable').DataTable({
    	
    	//responsive: true
    
         "scrollX": true
    
    });
    
} );

</script>

</html>
</div> <!-- este div cierra es el del principio -->


<?php include "footer.php"; ?>