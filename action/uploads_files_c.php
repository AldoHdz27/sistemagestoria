<?php session_start();

include_once "../config/config.php";
require '../composer/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\Exception\AwsException;

  $datos = $con->query("SELECT * FROM tbl_keys WHERE ID = 1");
  $d = $datos->fetch_all(MYSQLI_ASSOC);

  $key = $d[0]['KEY_'];
  $secrectkey = $d[0]['SECRETKEY'];

  $S3_Options = [
    'version' => 'latest',
    'region' => 'us-west-2',
    'credentials' => [

      'key' => encrypt_decrypt('decrypt', $key),
      'secret' => encrypt_decrypt('decrypt', $secrectkey)
    ]
  ];

  $S3 = new S3Client($S3_Options);


/*



*/

date_default_timezone_set ('America/Mexico_City');

$user_id = $_SESSION['user_id'];
$datos = $con->query("SELECT * FROM tbl_usuarios WHERE ID = $user_id");
$d = $datos->fetch_object();
$correo = $d->EMAIL;

  $id=$_POST['id'];

  $id_str = strval($id);
  $folio = '0';

  while(strlen($folio.$id_str)  < 11 ){
    $folio = $folio.'0';
  }


$carpeta = '../storage/DOCUMENTOS_'.$folio.$id_str;
if (!file_exists($carpeta)) {
    mkdir($carpeta, 0777, true);
}

$filename = $_FILES['file']['name'];

$location = $carpeta.$filename;
$uploadOk = 1;
$imageFileType = pathinfo($location,PATHINFO_EXTENSION);



/* Valid Extensions */
$valid_extensions = array("jpg","jpeg","png","xls","pdf");
/* Check file extension */
if( !in_array(strtolower($imageFileType),$valid_extensions) ) {
   $uploadOk = 0;
}

if($uploadOk == 0){
   echo 0;
}else{
   /* Upload file */
    $extension=pathinfo($filename,PATHINFO_EXTENSION);

    $name_file = $folio.$id_str.'_'."Complemento_".date("ymdhis");

   if(move_uploaded_file($_FILES['file']['tmp_name'],$carpeta."/".$name_file.".".$extension)){

      $complete_name_file = $name_file.".".$extension;

       $uploadObject = $S3->putObject([
          'Bucket' => 'crfact',
          'Key' => $name_file.".".$extension,
          'SourceFile' => $carpeta."/".$name_file.".".$extension
      ]);

      $sql = "INSERT INTO tbl_bitacora_d(FOLIO, PATH, NOMBRE, USUARIO) VALUES($id,'$complete_name_file','$complete_name_file','$correo')";
      $con->query($sql);

      echo 1;
   }else{
      echo 0;
   }
}




?>