$(".btn-click-action").click(function() {

    var name= $(this).attr('name');
    var identificador = name.split("_");
     
    var valor1= "valor1_"+identificador[1];
    var valor2= "valor2_"+identificador[1];
    var valor3= "valor3_"+identificador[1];
    
    var nombre = $('.nombre_'+identificador[1]).val();
    var entidad = $('.entidad_'+identificador[1]).val();
    var vin = $('.vin_'+identificador[1]).val();
    
    if($("#"+valor1).val() == "" || $("#"+valor2).val() == "" || $("#"+valor3).val() == ""){
      swal("Campos Vacios", "Llene los campos de cotizacion", "info");
    }else{
      $.ajax({
          url: "action/updatelayout.php",
          type: 'POST',
         
          async : true,
          data:{ 
            'valor1' : $("#"+input_valor1).val(),
            'valor2' : $("#"+input_valor2).val(),
            'valor3' : $("#"+input_valor3).val(),
            'nombre' : nombre,  
            'entidad' : entidad,
            'vin' : vin,
            'id' : identificador[1]
           } ,
          success: function(resp) {
            swal("Datos Actualizados", "", "success");
          },
          error: function(request,err){
            console.log("error")
            console.log(err)
          }
      });
      
    }

  });


  $(".factura").change(function(){

    var identificador=$(this).attr('id');
    var data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
    var no_cliente = data[1];

    var archivos = new FormData();
    var factura = $('#Factura_'+no_cliente)[0].files[0];
    var tipo_unidad = $("#tipo_unidad_"+no_cliente).text();

    archivos.append('factura',factura);
    archivos.append('no_cliente',no_cliente);
    archivos.append('tipo_unidad',tipo_unidad);
    
    swal({
      title: factura.name,
      text: "¿Cargar archivo?",
      icon: "info",
      buttons: ["Cancelar",true],
      dangerMode: true,
    })
    .then((value) => {
      if (value){
        $.ajax({
            url: 'action/uploads_factura.php',
            type: 'post',
            data: archivos,
            contentType: false,
            processData: false,
            success: function(response){
                if(response > 0){
                  swal('Archivo cargado exitosamente','','success');
                }else{
                  swal({
                    title:'Error al cargar el archivo',
                    text: 'intentalo más tarde',
                    icon: 'error',
                    dangerMode: true,
                  });
                }
            },
        });      
      }else{
        $('#Factura_'+no_cliente).val('');
      }
    });
  
  });


  $(".comprobante").change(function(){

    var identificador=$(this).attr('id');
    var data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
    var no_cliente = data[1];
    var comprobante = $('#Comprobante_'+no_cliente)[0].files[0];
    
    swal({
      title: comprobante.name,
      text: "Archivo seleccionado",
      icon: "info",
      buttons: ["Cancelar",true],
      dangerMode: true,
    })
    .then((value) => {
      if (!value){
        $('#Factura_'+no_cliente).val('');
      }
    });
  
  });

    
  function mi_funcion(id){

    if($('#Comprobante_'+id).val() != ""){

      var archivos = new FormData();
      var comprobante = $('#Comprobante_'+id)[0].files[0];
      
      archivos.append('comprobante',comprobante);
      archivos.append('id',id);

      document.getElementById("loader").style.display = "block";

      $.ajax({
          url: 'action/uploads_files.php',
          type: 'post',
          data: archivos,
          contentType: false,
          processData: false,
          success: function(response){
              if(response > 0){
                   document.getElementById("form_"+id).submit();                      
              }else{
                  alert('file not uploaded');
              }
          },
      });

    }else{
      swal({
        title: "No se ha cargado Comprobante",
        text: "Agrege el documento",
        icon: "info",
        dangerMode: true,
      });
    }

  }

  function start_loader(){

    document.getElementById("loader").style.display = "block";

  }

  $("#carga_archivo").change(function(){

      var path= $(this).val();
      var name  = path.split('\\');
      var ext = name[2].split(".");

      if(ext[1] == 'xlsx'){
        console.log(ext[1]);

        $("#name_file").val("Archivo: "+name[2]);

        console.log(name);
        $("#btn-submit").attr("disabled",false);
        document.getElementById("div_file_name").style.display = "block";
       
      }else{
        console.log(ext[1]);
        $("#btn-submit").attr("disabled",true);
        swal('Formato no permitido','Cargue un documento .xlsx', 'error');

      }

  });

  $(".cotizar").click(function(){

    var name= $(this).attr('id');
    var identificador = name.split("_");

    var cotizacion1 = '';
    var cotizacion2 = '';
    var cotizacion3 = '';


    switch(identificador[0]){

      case "cotizacion1" :

        cotizacion2 = "cotizacion2_"+(parseInt(identificador[1])+1)+"_"+identificador[2];
        cotizacion3 = "cotizacion3_"+(parseInt(identificador[1])+2)+"_"+identificador[2];

        document.getElementById("cotizacion1_"+(parseInt(identificador[1]))+"_"+identificador[2]).disabled = true;
        if(document.getElementById(cotizacion2) != null){
          document.getElementById(cotizacion2).disabled = true;
        }
        if(document.getElementById(cotizacion3) != null){
          document.getElementById(cotizacion3).disabled = true;
        }
        
        break;

      case "cotizacion2" :

        cotizacion1 = "cotizacion1_"+(parseInt(identificador[1])-1)+"_"+identificador[2];
        cotizacion3 = "cotizacion3_"+(parseInt(identificador[1])+1)+"_"+identificador[2];

        document.getElementById("cotizacion2_"+(parseInt(identificador[1]))+"_"+identificador[2]).disabled = true;
        
        if(document.getElementById(cotizacion1) != null){
          document.getElementById(cotizacion1).disabled = true;
        }
        if(document.getElementById(cotizacion3) != null){
          document.getElementById(cotizacion3).disabled = true;
        }
        
        break;

      case "cotizacion3" :
        
        cotizacion1 = "cotizacion1_"+(parseInt(identificador[1])-2)+"_"+identificador[2];
        cotizacion2 = "cotizacion2_"+(parseInt(identificador[1])-1)+"_"+identificador[2];

        document.getElementById("cotizacion3_"+(parseInt(identificador[1]))+"_"+identificador[2]).disabled = true;
        if(document.getElementById(cotizacion1) != null){
          document.getElementById(cotizacion1).disabled = true;
        }
        if(document.getElementById(cotizacion2) != null){
          document.getElementById(cotizacion2).disabled = true;
        }

        break;

    }

    $("#label_"+identificador[2]).text("Se ha aceptado la cotización: "+identificador[1]);
    $("#label_"+identificador[2]).css("color", "green");

    
    document.getElementById("Factura_"+identificador[2]).disabled = false;
    document.getElementById("Comprobante_"+identificador[2]).disabled = false;
    document.getElementById("Guardar_"+identificador[2]).disabled = false;
    
       
    $.ajax({
          url: 'action/set_autorizacion.php',
          type: 'post',
          data: {'folio': identificador[1],
                'no_cliente' : identificador[2]
                },
          success: function(response){
              
          },
      });

  });

  $("#myTable").DataTable({
    
  });