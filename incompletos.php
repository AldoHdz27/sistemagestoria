
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.js" ></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>

<?php 
  
  

    $active="active"; 
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 

    if ($_SESSION['usuario'] != 'Comercial') {
    session_destroy();
    $_SESSION = array();

    header('Location: index.php'); 
    }

    function alert($msg,$val) {

        if($val == 1){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','success');</script>";
        }else if($val == 2){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','info');</script>";
        }else if($val == 3){
             echo "<script type='text/javascript'>swal('Archivo Imcopatible o vacio','$msg','error');</script>";
        }else if($val == 4){
           echo "<script type='text/javascript'>swal('Comprobante cargado y Registro movido','$msg','success');</script>";
        }else{
          echo "<script type='text/javascript'>swal('Registro rechazado','$msg','error');</script>";
        }        
    
    }

    if(empty($_GET)){
        
    }else{

        if(!empty($_GET['success'])){

          switch ($_GET['success']) {
              case 'true':
                  alert("Se han cargado exitosamente",1); 
                  break;
              case 'false':
                  alert("No se pudo guradar el archivo ",2);
                  break;
              case 'error':
                  alert("Revise el formato (Columnas del archivo)",3);
                  break;
          }
        
        }

        if(!empty($_GET['move_success'])){

          switch ($_GET['move_success']) {
              case 'true':
                  alert("A tabla En proceso",4); 
                  break;
              case 'false':
                  alert("",5);
                  break;
          }
        
        }
    
    }

?>

<div class="content-wrapper" >
  <section class="content-header" hidden>
    <h1></h1>
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Incompletos</li>
    </ol>
  </section>

        
<?php

include_once "config/config.php";
$datos = $con->query("SELECT * FROM v_revision_i ");
//$datos_cotizaciones = $con->query("select * from v_revision_i");
$activos= $con->query("SELECT COUNT(*) Abiertos FROM db_sistema.tbl_staging;");

?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
<h1 style="padding-top: 25px;">&nbsp;&nbsp;INCOMPLETOS</h1>

  <div id="loader" style="display: none" ></div>

<?php if($datos->num_rows > 0):?>
  
  <div class="container-fluid" >
  <div class="row" id="tabla">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
  <table border="1"id="myTable"class="table-bordered table-hover display compact" style="width:100%" >
  <thead>
    <th style="text-align:center">Fecha</th>
    <th style="text-align:center">N°cliente</th>
    <th style="text-align:center">Tipo</th>  
    <th style="text-align:center">Marca</th>
    <th  style="text-align:center">Ejecutivo</th>
   
    <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Comprobante</th>
    <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Guardar</th>
    <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Rechazar</th>
    <th <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Actualizar</th>
  </thead>
  <tbody>
    <?php while($d= $datos->fetch_object()):?>

    <?php 

    $sql_cotizaciones = "SELECT * FROM v_cotizaciones v WHERE v.ID = $d->ID";

    $result = mysqli_query($con, $sql_cotizaciones);
    $cotizaciones=mysqli_fetch_all($result, MYSQLI_ASSOC);

    switch (count($cotizaciones)) {
      case 1:
          $cotizacion1=$cotizaciones[0];
          $cotizacion2 = null;
          $cotizacion3 = null;

          $cotizacion2_visibility = 'hidden';
          $cotizacion3_visibility = 'hidden';
        break;
      
      case 2:
          $cotizacion1=$cotizaciones[0];
          $cotizacion2=$cotizaciones[1];
          $cotizacion3 = null;

          $cotizacion2_visibility = '';
          $cotizacion3_visibility = 'hidden';
        break;

      case 3:
          $cotizacion1=$cotizaciones[0];
          $cotizacion2=$cotizaciones[1];
          $cotizacion3=$cotizaciones[2];
        break;
    }
    ?>

    <tr align="center">
    <td >
      <?php 
        echo 
        '<a data-toggle="modal" href="#Modal_datos_'.$d->NO_CLIENTE.'"><i class="fa fa-plus-circle  icon" aria-hidden="true"></i></a>
        <div class="modal fade" id="Modal_datos_'.$d->NO_CLIENTE.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                            </button>
                             <h3 class="modal-title" id="myModalLabel"><b>'.$d->NOMBRE.'</b></h3>

                        </div>
                        <div class="modal-body" style="text-align:left;">
                         <ul>
                          <li>MODELO: '.$d->MODELO.'</li>
                          <li>VERSI&Oacute;N: '.$d->VERSION.'</li>
                          <li>AÑO: '.$d->ANIO.'</li>
                          <li>VALOR FACTURA: '.'$'.number_format($d->VALOR_FACTURA, 2).'</li>
                          <li>ESTADO ORIGEN: '.$d->EDO_ORIGEN.'</li>
                          <li>PDV: '.$d->PDV.'</li>
                          <li>VENDEDOR: '.$d->VENDEDOR.'</li>
                         </ul>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
      ?>
      <?php echo "&nbsp".$d->FECHA; ?>
    </td>

    <td class="numero_cliente"><?php echo $d->NO_CLIENTE; ?></td>
    <td  id="<?php echo "tipo_unidad_$d->NO_CLIENTE"; ?>" class="<?php echo "vin_".$d->NO_CLIENTE; ?>"><?php echo $d->TIPO; ?></td>
    <td class="<?php echo "nombre_".$d->NO_CLIENTE; ?>"><?php echo $d->MARCA; ?></td>
    <td class="<?php echo "entidad_".$d->NO_CLIENTE; ?>"><?php echo $d->EJECUTIVO; ?></td>
  
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> >
          <label class="custom-file-upload ">
          <input type="file" name="comprobante" class="comprobante" id="<?php echo "Comprobante_".$d->NO_CLIENTE; ?>" accept="application/pdf">
          <i class="fa fa-cloud-upload"></i>
          </label>
    </td>
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>  >
      <?php
        echo'
        <form id="form_'.$d->NO_CLIENTE.'" action="changeStatus_revision.php" method="POST" style="display:block; margin:auto;">
          <input type="text" id="ID_'.$d->NO_CLIENTE.'" name="ID" value="'.$d->ID.'" hidden >
          <input type="text" name="value_estatus" value="20" hidden>
        </form>
        <button class="btn btn-primary btn-success" type="button" id="Guardar_'.$d->NO_CLIENTE.'" onclick="mi_funcion('.$d->NO_CLIENTE.')"
          ><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
       
        ';
      ?>
    </td>
    <td <?php  if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>>
      <?php
        echo '
        <form id="formRechazar_'.$d->NO_CLIENTE.'" action="rechazados2.php" method="post" style="display:block; margin:auto;">
        <input type="text" name="numero_cliente" value="'.$d->NO_CLIENTE.'" hidden >
        <input type="text" name="ID" value="'.$d->ID.'" hidden >
        <input type="text" name="vista" value="imcompletos" hidden >
        </form>    
        <button class="btn btn-danger" onclick="return messageRechazar('.$d->NO_CLIENTE.'); return false;"
          ><i class="fa fa-times" aria-hidden="true"></i></button> 
        ';  
      ?>         
    </td>
     <td <?php  if($_SESSION["usuario"]!="Admin_JJ"){echo "hidden";} ?> >
      <?php  
        echo 
        '<button class="btn-click-action btn btn-success" id="button_'.$d->NO_CLIENTE.'" name="button_'.$d->NO_CLIENTE.'"
        ><i class="fa fa-refresh" ></i></button>
        ';  
      ?>
    </td>

  <?php endwhile; ?>
  </tbody>
</table>
</div>
</div>
</div>
</div>
<?php else:?>
  <h3 style="padding-left:15px;">NO SE HAN CARGADO DATOS</h3>
<?php endif; ?>
  
</body>

</html>
 
<script type="text/javascript">

  $(".comprobante").change(function(){

    var identificador=$(this).attr('id');
    var data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
    var no_cliente = data[1];
    var comprobante = $('#Comprobante_'+no_cliente)[0].files[0];
    
    swal({
      title: comprobante.name,
      text: "Archivo seleccionado",
      icon: "info",
      buttons: ["Cancelar",true],
      dangerMode: true,
    })
    .then((value) => {
      if (!value){
        $('#Comprobante_'+no_cliente).val('');
      }
    });
  
  });

    
  function mi_funcion(no_cliente){

    if($('#Comprobante_'+no_cliente).val() != ""){

      var archivos = new FormData();
      var comprobante = $('#Comprobante_'+no_cliente)[0].files[0];
      var id = $('#ID_'+no_cliente).val();

      console.log(id);

      archivos.append('file',comprobante);
      archivos.append('no_cliente',no_cliente);
      archivos.append('id', id);

      document.getElementById("loader").style.display = "block";

      $.ajax({
          url: 'action/uploads_files_c.php',
          type: 'post',
          data: archivos,
          contentType: false,
          processData: false,
          success: function(response){
              if(response > 0){
                   document.getElementById("form_"+no_cliente).submit();                      
              }else{
                  alert('file not uploaded');
              }
          },
      });

    }else{
      swal({
        title: "No se ha cargado Comprobante",
        text: "Agrege el documento",
        icon: "info",
        dangerMode: true,
      });
    }

  }

  function start_loader(){

    document.getElementById("loader").style.display = "block";

  }

  function messageRechazar(no_cliente){
   
    swal({
    title: '',
    text: "¿Desea rechazar este registro?",
    icon: "info",
    buttons: ["Cancelar",true],
    dangerMode: true,
    })
    .then((value) => {
      if (value){
        document.getElementById("formRechazar_"+no_cliente).submit();        
      }
    });

  }

  $("#myTable").DataTable({
    
  });


</script>

</div> <!-- este div cierra es el del principio -->

<?php include "footer.php"; ?>
