<?php 

  require 'PHPExcel/Classes/PHPExcel.php';
  include_once "config/config.php";

  $sql_cotizaciones = "SELECT * FROM v_clientes C";
  $result = mysqli_query($con, $sql_cotizaciones);
  $cotizaciones=mysqli_fetch_all($result, MYSQLI_ASSOC);

  $objPHPExcel = new PHPExcel();

  $objPHPExcel->getProperties()
  ->setCreator('Aldo Rodrigo')
  ->setTitle('Example Excel with PHP')
  ->setDescription('Example Document')
  ->setKeywords('excel phpexcel php')
  ->setCategory('Examples');

  $objPHPExcel->setActiveSheetindex(0);
  $objPHPExcel->getActiveSheet()->setTitle('First Sheet');
  $from = "A1"; // or any value
  $to = "Q1"; // or any value
  $objPHPExcel->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold( true );
  $objPHPExcel->getActiveSheet()
        ->getStyle("$from:$to")
        ->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('FFFF00');

    $objPHPExcel->getActiveSheet()->setCellValue('A'.(1),'NO_CLIENTE');
    $objPHPExcel->getActiveSheet()->setCellValue('B'.(1), 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('C'.(1), 'RFC');
    $objPHPExcel->getActiveSheet()->setCellValue('D'.(1), 'TIPO');
    $objPHPExcel->getActiveSheet()->setCellValue('E'.(1), 'CALLE');
    $objPHPExcel->getActiveSheet()->setCellValue('F'.(1), 'N_INT');
    $objPHPExcel->getActiveSheet()->setCellValue('G'.(1), 'N_EXT');
    $objPHPExcel->getActiveSheet()->setCellValue('H'.(1), 'COLONIA');
    $objPHPExcel->getActiveSheet()->setCellValue('I'.(1), 'MUNICIPIO');
    $objPHPExcel->getActiveSheet()->setCellValue('J'.(1), 'CIUDAD');
    $objPHPExcel->getActiveSheet()->setCellValue('K'.(1), 'CP');
    $objPHPExcel->getActiveSheet()->setCellValue('L'.(1), 'ESTADO');
    $objPHPExcel->getActiveSheet()->setCellValue('M'.(1), 'TEL_CASA');
    $objPHPExcel->getActiveSheet()->setCellValue('N'.(1), 'TEL_TRABAJO');
    $objPHPExcel->getActiveSheet()->setCellValue('O'.(1), 'TEL_OTRO');
    $objPHPExcel->getActiveSheet()->setCellValue('P'.(1), 'TEL_CELULAR');
    $objPHPExcel->getActiveSheet()->setCellValue('Q'.(1), 'EMAIL');     

    for($i=0; $i< count($cotizaciones); $i++){

      $objPHPExcel->getActiveSheet()->setCellValue('A'.($i+2), $cotizaciones[$i]['NO_CLIENTE']);
      $objPHPExcel->getActiveSheet()->setCellValue('B'.($i+2), $cotizaciones[$i]['NOMBRE']);
      $objPHPExcel->getActiveSheet()->setCellValue('C'.($i+2), $cotizaciones[$i]['RFC']);
      $objPHPExcel->getActiveSheet()->setCellValue('D'.($i+2), $cotizaciones[$i]['TIPO']);
      $objPHPExcel->getActiveSheet()->setCellValue('E'.($i+2), $cotizaciones[$i]['CALLE']);
      $objPHPExcel->getActiveSheet()->setCellValue('F'.($i+2), $cotizaciones[$i]['N_INT']);
      $objPHPExcel->getActiveSheet()->setCellValue('G'.($i+2), $cotizaciones[$i]['N_EXT']);
      $objPHPExcel->getActiveSheet()->setCellValue('H'.($i+2), $cotizaciones[$i]['COLONIA']);
      $objPHPExcel->getActiveSheet()->setCellValue('I'.($i+2), $cotizaciones[$i]['MUNICIPIO']);
      $objPHPExcel->getActiveSheet()->setCellValue('J'.($i+2), $cotizaciones[$i]['CIUDAD']);
      $objPHPExcel->getActiveSheet()->setCellValue('K'.($i+2), $cotizaciones[$i]['CP']);
      $objPHPExcel->getActiveSheet()->setCellValue('L'.($i+2), $cotizaciones[$i]['ESTADO']);
      $objPHPExcel->getActiveSheet()->setCellValue('M'.($i+2), $cotizaciones[$i]['TEL_CASA']);
      $objPHPExcel->getActiveSheet()->setCellValue('N'.($i+2), $cotizaciones[$i]['TEL_TRABAJO']);
      $objPHPExcel->getActiveSheet()->setCellValue('O'.($i+2), $cotizaciones[$i]['TEL_OTRO']);
      $objPHPExcel->getActiveSheet()->setCellValue('P'.($i+2), $cotizaciones[$i]['TEL_CELULAR']);
      $objPHPExcel->getActiveSheet()->setCellValue('Q'.($i+2), $cotizaciones[$i]['EMAIL']);     

    }

  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment;filename="Datos.xlsx');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $objWriter->save('php://output');
  

?>