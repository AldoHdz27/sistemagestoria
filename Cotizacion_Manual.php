
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.js" ></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>

<?php 
    $active="active"; 
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 

    function alert($msg,$val) {

        if($val == 1){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','success');</script>";
        }else if($val == 2){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','info');</script>";
        }else if($val == 3){
             echo "<script type='text/javascript'>swal('Archivo Imcopatible o vacio','$msg','error');</script>";
        }else if($val == 4){
           echo "<script type='text/javascript'>swal('Comprobante cargado y Registro movido','$msg','success');</script>";
        }else{
          echo "<script type='text/javascript'>swal('Registro rechazado','$msg','error');</script>";
        }        
    
    }

    if(empty($_GET)){
        
    }else{

        if(!empty($_GET['success'])){

          switch ($_GET['success']) {
              case 'true':
                  alert("Se han cargado exitosamente",1); 
                  break;
              case 'false':
                  alert("No se pudo guradar el archivo ",2);
                  break;
              case 'error':
                  alert("Revise el formato (Columnas del archivo)",3);
                  break;
          }
        
        }

        if(!empty($_GET['move_success'])){

          switch ($_GET['move_success']) {
              case 'true':
                  alert("A tabla En proceso",4); 
                  break;
              case 'false':
                  alert("",5);
                  break;
          }
        
        }
    
    }

?>

<div class="content-wrapper" >
  <section class="content-header" hidden>
    <h1></h1>
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Importar Activaciones</li>
    </ol>
  </section>

        
<?php

include_once "config/config.php";
$datos = $con->query("SELECT * FROM v_cotizacion WHERE MANUAL = 1 and ACTUALIZADO = 0; ");
$datos_cotizaciones = $con->query("select * from v_cotizacion where NO_CLIENTE <> '' ");

?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>

  <h1 style="padding-top: 25px;">&nbsp;&nbsp;COTIZACIONES MANUALES</h1>

  <div id="loader" style="display: none" ></div>

  <div style="padding-top: 50px; padding-bottom: 15px; padding-left:25px; font-size: 20px;">
    <form method="post" id="addproduct" action="import_4.php" enctype="multipart/form-data" role="form">
      <div <?php if($usuario == 0 && $comercial == 0 && $admin_crfact == 0){echo "hidden";} ?> >
        <div class="row">
          <label class="custom-file-upload btn-info" style="padding-bottom: 10px; border-radius: 5px; border:solid 1px; ">
          <input type="file" name="name" class="custom-file-upload" id="carga_archivo" placeholder="Archivo (.xlsx)">
            <i class="fa fa-cloud-upload"></i>&nbsp; &nbsp;Cargar Archivo
          </label>
          <button type="submit" class="btn btn-success" id="btn-submit" style="font-size: 20px;" onclick="start_loader()" ><i class="fa fa-cloud-download" aria-hidden="true">
          </i><b>&nbsp; &nbsp;Importar Datos</b></button>
        </div>
      </div>
      <div class="row" id="div_file_name" style="display: none; width: 500px;" >
        <input type="text" id="name_file" style="width: 390px;" disabled>
      </div>
    </form>
  </div>

<?php if($datos->num_rows > 0):?>
  
  <div class="container-fluid" >
  <div class="row" id="tabla">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
  <table border="1"id="myTable"class="table-bordered table-hover display compact" style="width:100%" >
  <thead>
    <th style="text-align:center">Fecha</th>
    <th style="text-align:center">N°cliente</th>
    <th style="text-align:center">Tipo</th>  
    <th style="text-align:center">Marca</th>
    <th  style="text-align:center">Ejecutivo</th>
    <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Factura</th>
    <th  style="text-align:center">Proceso de Cotizaci&oacute;n</th>
    
    <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Comprobante</th>
    <th style="text-align:center">Guardar</th>
    <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Rechazar</th>
   
  <tbody>
    <?php while($d= $datos->fetch_object()):?>

    <?php 

    $sql_cotizaciones = "SELECT * FROM v_cotizaciones v WHERE v.ID = $d->ID ORDER BY FOLIO";

    $result = mysqli_query($con, $sql_cotizaciones);
    $cotizaciones=mysqli_fetch_all($result, MYSQLI_ASSOC);

    switch (count($cotizaciones)) {
      case 1:
          $cotizacion1=$cotizaciones[0];
          $cotizacion2 = null;
          $cotizacion3 = null;

          $cotizacion2_visibility = 'hidden';
          $cotizacion3_visibility = 'hidden';

          $cotizacion1_tooltips = mysqli_query($con, "SELECT * FROM v_info i WHERE i.EDO = '$cotizacion1[EDO_COTIZACION]' ");


          if($cotizacion1_tooltips){
            $data1_tooltips = $cotizacion1_tooltips->fetch_assoc();
            $tooltip1 = $data1_tooltips["OBSERVACIONES"];
          }else{
            $tooltip1 = '';
          }

          $tooltip2 = '';
          $tooltip3 = '';

        break;
      
      case 2:
          $cotizacion1=$cotizaciones[0];
          $cotizacion2=$cotizaciones[1];
          $cotizacion3 = null;

          $cotizacion2_visibility = '';
          $cotizacion3_visibility = 'hidden';

          $cotizacion1_tooltips = mysqli_query($con, "SELECT * FROM v_info i WHERE i.EDO = '$cotizacion1[EDO_COTIZACION]' ");


          if($cotizacion1_tooltips){

            $data1_tooltips = $cotizacion1_tooltips->fetch_assoc();

            $tooltip1 = $data1_tooltips["OBSERVACIONES"];

          }else{

            $tooltip1 = '';

          }

          $cotizacion2_tooltips = mysqli_query($con, "SELECT * FROM v_info i WHERE i.EDO = '$cotizacion2[EDO_COTIZACION]' ");

          if($cotizacion2_tooltips){

            $data2_tooltips = $cotizacion2_tooltips->fetch_assoc();

            $tooltip2 = $data2_tooltips["OBSERVACIONES"];

          }else{

            $tooltip2 = '';

          }

        $tooltip3 = '';

        break;

      case 3:
          $cotizacion1=$cotizaciones[0];
          $cotizacion2=$cotizaciones[1];
          $cotizacion3=$cotizaciones[2];

          $cotizacion2_visibility = '';
          $cotizacion3_visibility = '';

          $cotizacion1_tooltips = mysqli_query($con, "SELECT * FROM v_info i WHERE i.EDO = '$cotizacion1[EDO_COTIZACION]'");

          if($cotizacion1_tooltips){

            $data1_tooltips = $cotizacion1_tooltips->fetch_assoc();

            $tooltip1 = $data1_tooltips["OBSERVACIONES"];

          }else{

            $tooltip1 = '';

          }

          $cotizacion2_tooltips = mysqli_query($con, "SELECT * FROM v_info i WHERE i.EDO = '$cotizacion2[EDO_COTIZACION]'");

          if($cotizacion2_tooltips){

            $data2_tooltips = $cotizacion2_tooltips->fetch_assoc();

            $tooltip2 = $data2_tooltips["OBSERVACIONES"];

          }else{

            $tooltip2 = '';

          }

          $cotizacion3_tooltips = mysqli_query($con, "SELECT * FROM v_info i WHERE i.EDO = '$cotizacion3[EDO_COTIZACION]'");          

          if($cotizacion3_tooltips){

            $data3_tooltips = $cotizacion3_tooltips->fetch_assoc();

            $tooltip3 = $data3_tooltips["OBSERVACIONES"];

          }else{

            $tooltip3 = '';

          }       

        break;
    }

    ?>

    <tr align="center">
    <td >
      <?php 
        echo 
        '<a data-toggle="modal" href="#Modal_datos_'.$d->NO_CLIENTE.'"><i class="fa fa-plus-circle  icon" aria-hidden="true"></i></a>
        <div class="modal fade" id="Modal_datos_'.$d->NO_CLIENTE.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                            </button>
                             <h3 class="modal-title" id="myModalLabel"><b>'.$d->NOMBRE.'</b></h3>

                        </div>
                        <div class="modal-body" style="text-align:left;">
                         <ul>
                          <li>MODELO: '.$d->MODELO.'</li>
                          <li>VERSI&Oacute;N: '.$d->VERSION.'</li>
                          <li>AÑO: '.$d->ANIO.'</li>
                          <li>VALOR FACTURA: '.'$'.number_format($d->VALOR_FACTURA, 2).'</li>
                          <li>ESTADO ORIGEN: '.$d->EDO_ORIGEN.'</li>
                          <li>PDV: '.$d->PDV.'</li>
                          <li>VENDEDOR: '.$d->VENDEDOR.'</li>
                         </ul>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
      ?>
      <?php echo "&nbsp".$d->FECHA; ?>
    </td>

    <td class="numero_cliente"><?php echo $d->NO_CLIENTE; ?></td>
    <td  id="<?php echo "tipo_unidad_$d->NO_CLIENTE"; ?>" class="<?php echo "vin_".$d->NO_CLIENTE; ?>"><?php echo $d->TIPO; ?></td>
    <td class="<?php echo "nombre_".$d->NO_CLIENTE; ?>"><?php echo $d->MARCA; ?></td>
    <td class="<?php echo "entidad_".$d->NO_CLIENTE; ?>"><?php echo $d->EJECUTIVO; ?></td>
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> >
        <label class=" custom-file-upload">
        <input type="file" class="factura" accept="application/pdf" id="Factura_<?php echo $d->NO_CLIENTE; ?>" >
        <i class="fa fa-cloud-upload"></i>
        </label>
    </td>
    <td class="estado">
      <?php
        echo 
        '<button class="btn-default popup" id="btn-modal_'.$d->NO_CLIENTE.'" 
        data-toggle="modal" data-target="#myModal_'.$d->NO_CLIENTE.'" style="width: 80px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </button>

        <div class="modal fade" id="myModal_'.$d->NO_CLIENTE.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="vertical-alignment-helper">
              <div class="modal-dialog vertical-align-center">
                  <div class="modal-content" style=" min-width:900px !important;">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                          </button>
                         
                          <h4 class="modal-title" id="myModalLabel"><b>Iniciar Proceso de cotizaci&oacute;n: '.$d->NOMBRE.'</b></h4>

                      </div>
                      <div class="container text-left" >
                          <label class="modal-title" id="label_'.$d->NO_CLIENTE.'"></label>
                      </div>
                      <div class="modal-body">
                        <table border="1" class="table-bordered table-hover" style="width:100%">
                          <thead>
                            <th  style="text-align:center; background-color:#00c0ef;">Cotizaci&oacute;n</th>
                            <th style="text-align:center; background-color: #00c0ef;">Estado</th>
                            <th style="text-align:center; background-color: #00c0ef;">Tipo</th>
                            
                            <th  style="text-align:center; background-color:#00c0ef;">SLA</th>
                            <th style="text-align:center; background-color: #00c0ef;">SLA_AVG</th>
                            
                            <th style="text-align:center; background-color: #00c0ef;">Derechos</th>
                            <th style="text-align:center; background-color: #00c0ef;">Tenencia</th>
                            <th style="text-align:center; background-color: #00c0ef;">Tarjeta</th>
                            <th style="text-align:center; background-color: #00c0ef;">Otro costo</th>
                            <th style="text-align:center; background-color: #00c0ef;">Total</th>
                            <th style="text-align:center; background-color: #00c0ef;">Estatus</th>
                            <th style="text-align:center;"></th>
                          </thead>
                          <tbody>
                            <tr style="text-align:center">
                            <td>'.$cotizacion1['FOLIO'].'</td>
                            <td><span data-toggle="tooltip" data-placement="left" title="'.$tooltip1.'"><u>'.$cotizacion1['EDO_COTIZACION'].'</u></span></td>
                            <td>'.$cotizacion1['AUTOMATICA'].'</td>
                            <td>'.$cotizacion1['SLA'].'</td>
                            <td>'.$cotizacion1['SLA_AVG'].'</td>';
                            
                            if($cotizacion1['AUTOMATICA'] == 'M' && $cotizacion1['ACTUALIZADO'] == 0 ){
                              echo '
                                <td><input type="text" id="derechos_cotizacion1_'.$d->NO_CLIENTE.'" maxlength="6" value="'.number_format($cotizacion1['MTO_DERECHOS'],2).'" size="4" class="cotizacion cotizacion1"'; echo '></td>
                                <td><input type="text" id="tenencia_cotizacion1_'.$d->NO_CLIENTE.'" maxlength="6" value="'.number_format($cotizacion1['MTO_TENENCIA'],2).'" size="4" class="cotizacion cotizacion1"></td>
                                <td><input type="text" id="tarjeta_cotizacion1_'.$d->NO_CLIENTE.'" maxlength="6" value="'.number_format($cotizacion1['MTO_TARJETA'],2).'" size="4" class="cotizacion cotizacion1"></td>
                                <td><input type="text" id="otros_cotizacion1_'.$d->NO_CLIENTE.'" maxlength="6" value="'.number_format($cotizacion1['MTO_OTRO'],2).'" size="4" class="cotizacion cotizacion1"></td>
                                <td id="total_cotizacion1_'.$d->NO_CLIENTE.'" >'.number_format($cotizacion1['MTO_TOTAL'],2).'</td>
                              ';
                            }else{
                              echo '
                                <td>'.'$'.number_format($cotizacion1['MTO_DERECHOS'],2).'</td>
                                <td>'.'$'.number_format($cotizacion1['MTO_TENENCIA'],2).'</td>
                                <td>'.'$'.number_format($cotizacion1['MTO_TARJETA'],2).'</td>
                                <td>'.'$'.number_format($cotizacion1['MTO_OTRO'],2).'</td>
                                <td>'.'$'.number_format($cotizacion1['MTO_TOTAL'],2).'</td>
                              ';
                            }
                            
                            echo '
                            <td id="est_cotizacion1">'.$cotizacion1['ESTATUS'].'</td>
                            <td>
                              <button class="cotizar btn btn-default"';
                                if($cotizacion1['FOLIO'] == '' || $cotizacion1['MTO_TOTAL'] == 0 || $cotizacion1['AUTOMATICA'] == 'A' || ($cotizacion1['ACTUALIZADO'] == 1 && $cotizacion1['AUTOMATICA'] == 'M')){echo "disabled ";}
                                  echo 'id="cotizacion1_'.$cotizacion1['FOLIO'].'_'.$d->NO_CLIENTE.'">
                                  <i class="fa fa-check" aria-hidden="true"></i>
                              </button>
                            </td>
                            </tr>
                            <tr style="text-align:center" '.$cotizacion2_visibility.'>
                            <td>'.$cotizacion2['FOLIO'].'</td>
                            <td><span data-toggle="tooltip" data-placement="left" title="'.$tooltip2.'"><u>'.$cotizacion2['EDO_COTIZACION'].'</u></span></td>
                            <td>'.$cotizacion2['AUTOMATICA'].'</td>
                            <td>'.$cotizacion2['SLA'].'</td>
                            <td>'.$cotizacion2['SLA_AVG'].'</td>';

                            if($cotizacion2['AUTOMATICA'] == 'M' && $cotizacion2['ACTUALIZADO'] == 0){
                              echo '
                                <td><input type="text" id="derechos_cotizacion2_'.$d->NO_CLIENTE.'" maxlength="6" size="4" value="'.number_format($cotizacion2['MTO_DERECHOS'],2).'" class="cotizacion cotizacion2"'; if($cotizacion2['MTO_DERECHOS'] > 0 ){echo 'disabled ';} echo '></td>
                                <td><input type="text" id="tenencia_cotizacion2_'.$d->NO_CLIENTE.'" maxlength="6" size="4" value="'.number_format($cotizacion2['MTO_TENENCIA'],2).'" class="cotizacion cotizacion2"></td>
                                <td><input type="text" id="tarjeta_cotizacion2_'.$d->NO_CLIENTE.'" maxlength="6" size="4" value="'.number_format($cotizacion2['MTO_TARJETA'],2).'" class="cotizacion cotizacion2"></td>
                                <td><input type="text" id="otros_cotizacion2_'.$d->NO_CLIENTE.'" maxlength="6" size="4" value="'.number_format($cotizacion2['MTO_OTRO'],2).'" class="cotizacion  cotizacion2"></td>
                                <td id="total_cotizacion2_'.$d->NO_CLIENTE.'">'.number_format($cotizacion2['MTO_TOTAL'],2).'</td>
                              ';
                            }else{
                              echo '
                                <td>'.'$'.number_format($cotizacion2['MTO_DERECHOS'],2).'</td>
                                <td>'.'$'.number_format($cotizacion2['MTO_TENENCIA'],2).'</td>
                                <td>'.'$'.number_format($cotizacion2['MTO_TARJETA'],2).'</td>
                                <td>'.'$'.number_format($cotizacion2['MTO_OTRO'],2).'</td>
                                <td>'.'$'.number_format($cotizacion2['MTO_TOTAL'],2).'</td>
                              ';
                            }
                            echo'
                            <td id="est_cotizacion2">'.$cotizacion2['ESTATUS'].'</td>
                            <td>
                              <button class="cotizar btn btn-default"';
                                if(!isset($cotizacion2)){
                                  echo "style='display:none' ";
                                }else if($cotizacion2['FOLIO'] == '' || $cotizacion2['AUTOMATICA'] == 'A' || ($cotizacion2['ACTUALIZADO'] == 1 && $cotizacion2['AUTOMATICA'] == 'M')){
                                  echo "disabled ";
                                }
                                  echo 'id="cotizacion2_'.$cotizacion2['FOLIO'].'_'.$d->NO_CLIENTE.'">
                                  <i class="fa fa-check" aria-hidden="true"></i>
                              </button>
                            </tr>
                            <tr style="text-align:center" '.$cotizacion3_visibility.'>
                            <td>'.$cotizacion3['FOLIO'].'</td>
                            <td><span data-toggle="tooltip" data-placement="left" title="'.$tooltip3.'"><u>'.$cotizacion3['EDO_COTIZACION'].'</u></span></td>
                            <td>'.$cotizacion3['AUTOMATICA'].'</td>
                            <td>'.$cotizacion3['SLA'].'</td>
                            <td>'.$cotizacion3['SLA_AVG'].'</td>';

                            if($cotizacion3['AUTOMATICA'] == 'M' && $cotizacion3['ACTUALIZADO'] == 0){
                              echo '
                                <td><input type="text" id="derechos_cotizacion3_'.$d->NO_CLIENTE.'" maxlength="6" value="'.number_format($cotizacion3['MTO_DERECHOS'],2).'" size="4" class="cotizacion cotizacion3"></td>
                                <td><input type="text" id="tenencia_cotizacion3_'.$d->NO_CLIENTE.'" maxlength="6" value="'.number_format($cotizacion3['MTO_TENENCIA'],2).'" size="4" class="cotizacion cotizacion3"></td>
                                <td><input type="text" id="tarjeta_cotizacion3_'.$d->NO_CLIENTE.'" maxlength="6" value="'.number_format($cotizacion3['MTO_TARJETA'],2).'" size="4" class="cotizacion cotizacion3"></td>
                                <td><input type="text" id="otros_cotizacion3_'.$d->NO_CLIENTE.'" maxlength="6" value="'.number_format($cotizacion3['MTO_OTRO'],2).'" size="4" class="cotizacion cotizacion3"></td>
                                <td id="total_cotizacion3_'.$d->NO_CLIENTE.'">'.number_format($cotizacion3['MTO_TOTAL'],2).'</td>
                              ';
                            }else{
                              echo '
                                <td>'.'$'.number_format($cotizacion3['MTO_DERECHOS'],2).'</td>
                                <td>'.'$'.number_format($cotizacion3['MTO_TENENCIA'],2).'</td>
                                <td>'.'$'.number_format($cotizacion3['MTO_TARJETA'],2).'</td>
                                <td>'.'$'.number_format($cotizacion3['MTO_OTRO'],2).'</td>
                                <td>'.'$'.number_format($cotizacion3['MTO_TOTAL'],2).'</td>
                              ';
                            }
                            echo'
                            <td id="est_cotizacion3">'.$cotizacion3['ESTATUS'].'</td>
                            <td>
                              <button class="cotizar btn btn-default"';
                                if(!isset($cotizacion3)){
                                  echo "style='display:none' ";
                                }else if($cotizacion3['FOLIO'] == '' || $cotizacion3['AUTOMATICA'] == 'A'){
                                  echo "disabled ";
                                }
                                  echo 'id="cotizacion3_'.$cotizacion3['FOLIO'].'_'.$d->NO_CLIENTE. '">
                                  <i class="fa fa-check" aria-hidden="true"></i>
                              </button>
                            </tr>
                          </tbody>

                        </table>

                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                  </div>
              </div>
          </div>
        </div>
        ';
      ?>
   
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> >
          <label class="custom-file-upload ">
          <input type="file" class="comprobante" id="<?php echo "Comprobante_".$d->NO_CLIENTE; ?>" accept="application/pdf" disabled>
          <i class="fa fa-cloud-upload"></i>
          </label>
    </td>
    <td>
      <?php
        echo
        '<form id="form_'.$d->NO_CLIENTE.'" action="en_proceso.php" method="POST" style="display:block; margin:auto;">
          <input type="text" name="numero_cliente" value="'.$d->NO_CLIENTE.'" hidden>
          <input type="text" id="ID_'.$d->NO_CLIENTE.'" name="ID" value="'.$d->ID.'" hidden >
          <input type="text" name="nombre_cliente" value="'.$d->NOMBRE.'" hidden>
          <input type="text" name="agencia" value="'.$d->EDO_ORIGEN.'" hidden>
          <input type="text" name="unidad" value="'.$d->MARCA.'_'.$d->VERSION.'" hidden>
        </form>
        <button class="btn btn-primary" type="button" id="Guardar_'.$d->NO_CLIENTE.'" onclick="mi_funcion('.$d->NO_CLIENTE.')" disabled 
          ><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
        ';
      ?>
    </td>
    <td <?php  if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>>
      <?php
        echo '
        <form action="rechazados2.php" method="post" style="display:block; margin:auto;">
        <input type="text" name="numero_cliente" value="'.$d->NO_CLIENTE.'" hidden >
        <input type="text" name="ID" value="'.$d->ID.'" hidden >
        <button class="btn btn-danger" id="Rechazar_"'.$d->NO_CLIENTE.'"  
        ><i class="fa fa-times" aria-hidden="true"></i></button>
        </form>     
        ';  
      ?>         
    </td>

  <?php endwhile; ?>
  </tbody>
</table>
</div>
</div>
</div>
</div>
<?php else:?>
  <h3 style="padding-left:15px;">NO SE HAN CARGADO DATOS</h3>
<?php endif; ?>
  
</body>

</html>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
<script type="text/javascript">

  $("#myTable").DataTable({
    "destroy":true
  });
  
  $(".btn-click-action").click(function() {

    var name= $(this).attr('name');
    var identificador = name.split("_");
     
    var valor1= "valor1_"+identificador[1];
    var valor2= "valor2_"+identificador[1];
    var valor3= "valor3_"+identificador[1];
    
    var nombre = $('.nombre_'+identificador[1]).val();
    var entidad = $('.entidad_'+identificador[1]).val();
    var vin = $('.vin_'+identificador[1]).val();
    
    if($("#"+valor1).val() == "" || $("#"+valor2).val() == "" || $("#"+valor3).val() == ""){
      swal("Campos Vacios", "Llene los campos de cotizacion", "info");
    }else{
      $.ajax({
          url: "action/updatelayout.php",
          type: 'POST',
         
          async : true,
          data:{ 
            'valor1' : $("#"+input_valor1).val(),
            'valor2' : $("#"+input_valor2).val(),
            'valor3' : $("#"+input_valor3).val(),
            'nombre' : nombre,  
            'entidad' : entidad,
            'vin' : vin,
            'id' : identificador[1]
           } ,
          success: function(resp) {
            swal("Datos Actualizados", "", "success");
          },
          error: function(request,err){
            console.log("error")
            console.log(err)
          }
      });
      
    }

  });


  $(".factura").change(function(){

    var identificador=$(this).attr('id');
    var data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
    var no_cliente = data[1];

    var archivos = new FormData();
    var factura = $('#Factura_'+no_cliente)[0].files[0];
    var tipo_unidad = $("#tipo_unidad_"+no_cliente).text();
    //var id = $('#ID').val();
    var id = $('#ID_'+no_cliente).val();

    archivos.append('factura', factura);
    archivos.append('no_cliente', no_cliente);
    archivos.append('tipo_unidad', tipo_unidad);
    archivos.append('id', id);
    
    swal({
      title: factura.name,
      text: "¿Cargar archivo?",
      icon: "info",
      buttons: ["Cancelar",true],
      dangerMode: true,
    })
    .then((value) => {
      if (value){
        $.ajax({
            url: 'action/uploads_factura.php',
            type: 'post',
            data: archivos,
            contentType: false,
            processData: false,
            success: function(response){
                if(response > 0){
                  swal('Archivo cargado exitosamente','','success');
                  $('#Factura_'+no_cliente).val('');
                }else{
                  swal({
                    title:'Error al cargar el archivo',
                    text: 'intentalo más tarde',
                    icon: 'error',
                    dangerMode: true,
                  });
                }
            },
        });      
      }else{
        $('#Factura_'+no_cliente).val('');
      }
    });
  
  });


  $(".comprobante").change(function(){

    var identificador=$(this).attr('id');
    var data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
    var no_cliente = data[1];
    var comprobante = $('#Comprobante_'+no_cliente)[0].files[0];
    
    swal({
      title: comprobante.name,
      text: "Archivo seleccionado",
      icon: "info",
      buttons: ["Cancelar",true],
      dangerMode: true,
    })
    .then((value) => {
      if (!value){
        $('#Comprobante_'+no_cliente).val('');
      }
    });
  
  });

    
  function mi_funcion(no_cliente){

    if($('#Comprobante_'+no_cliente).val() != ""){

      var archivos = new FormData();
      var comprobante = $('#Comprobante_'+no_cliente)[0].files[0];
      var id = $('#ID_'+no_cliente).val();

      archivos.append('comprobante',comprobante);
      archivos.append('no_cliente',no_cliente);
      archivos.append('id', id);

      document.getElementById("loader").style.display = "block";

      $.ajax({
          url: 'action/uploads_files.php',
          type: 'post',
          data: archivos,
          contentType: false,
          processData: false,
          success: function(response){
              if(response > 0){
                   document.getElementById("form_"+no_cliente).submit();                      
              }else{
                  alert('file not uploaded');
              }
          },
      });

    }else{
      swal({
        title: "No se ha cargado Comprobante",
        text: "Agrege el documento",
        icon: "info",
        dangerMode: true,
      });
    }

  }

  function start_loader(){

    document.getElementById("loader").style.display = "block";

  }

  $("#carga_archivo").change(function(){

      var path= $(this).val();
      var name  = path.split('\\');
      var ext = name[2].split(".");

      if(ext[1] == 'xlsx'){
        console.log(ext[1]);

        $("#name_file").val("Archivo: "+name[2]);

        console.log(name);
        $("#btn-submit").attr("disabled",false);
        document.getElementById("div_file_name").style.display = "block";
       
      }else{
        console.log(ext[1]);
        $("#btn-submit").attr("disabled",true);
        swal('Formato no permitido','Cargue un documento .xlsx', 'error');

      }

  });

  $(".cotizar").click(function(){

    var name= $(this).attr('id');
    var identificador = name.split("_");
    
    document.getElementById(name).classList.remove("btn-secondary");
    document.getElementById(name).classList.add("btn-success");

    let derechos = 0;
    let tenencia = 0;
    let tarjeta = 0;
    let otros = 0;

    derechos = $('#derechos_'+identificador[0]+'_'+identificador[2]).val();
    tenencia = $('#tenencia_'+identificador[0]+'_'+identificador[2]).val();
    tarjeta = $('#tarjeta_'+identificador[0]+'_'+identificador[2]).val();
    otros = $('#otros_'+identificador[0]+'_'+identificador[2]).val();

    derechos = derechos.replace(/,/g, "");
    d = parseFloat(derechos);
    tenencia = tenencia.replace(/,/g, "");
    t = parseFloat(tenencia);
    tarjeta = tarjeta.replace(/,/g, "");
    tar = parseFloat(tarjeta);
    otros = otros.replace(/,/g, "");
    ot = parseFloat(otros);

    console.log(identificador);

    console.log(derechos);
    console.log(tenencia);
    console.log(tarjeta);
    console.log(otros);

    switch(identificador[0]){

      case "cotizacion1" :

        document.getElementById("cotizacion1_"+(parseInt(identificador[1]))+"_"+identificador[2]).disabled = true;

      
        break;

      case "cotizacion2" :

        document.getElementById("cotizacion2_"+(parseInt(identificador[1]))+"_"+identificador[2]).disabled = true;

        break;

      case "cotizacion3" :

        document.getElementById("cotizacion3_"+(parseInt(identificador[1]))+"_"+identificador[2]).disabled = true;

        break;

    }

    var id = $('#ID_'+identificador[2]).val();
    console.log(id);
    

    $.ajax({
          url: 'action/update_cotizacionesM.php',
          type: 'post',
          data: {'folio': identificador[1],
                'id' : id,
                'derechos' : d,
                'tenencia' : t,
                'tarjeta' : tar,
                'otros' : ot
                },
          success: function(response){

            $("#label_"+identificador[2]).text("Se ha actualizado la cotización: "+identificador[1]);
            $("#label_"+identificador[2]).css("color", "green");

            $('#derechos_'+identificador[0]).prop('disabled', true);
            $('#tenencia_'+identificador[0]).prop('disabled', true);
            $('#tarjeta_'+identificador[0]).prop('disabled', true);
            $('#otros_'+identificador[0]).prop('disabled', true);

            console.log(response);
      
          },
      });

  });


  $('.test-popup-link').magnificPopup({

    type: 'image'

  });




  $(".cotizacion1").change(function(){

    //let total = parseInt($(this).val());
    let idElement = $(this).attr('id');
    let attrElement = idElement.split("_");
    let numero_cliente = attrElement[2];
    console.log(numero_cliente);

    let derechos = 0;
    let tenencia = 0;
    let tarjeta = 0;
    let otros = 0;
    let total = 0;

    derechos = $('#derechos_cotizacion2_'+numero_cliente).val();
    derechos = derechos.replace(/,/g, "");
    d = parseFloat(derechos);
    tenencia = $('#tenencia_cotizacion2_'+numero_cliente).val();
    tenencia = tenencia.replace(/,/g, "");
    t = parseFloat(tenencia);
    tarjeta = $('#tarjeta_cotizacion2_'+numero_cliente).val();
    tarjeta = tarjeta.replace(/,/g, "");
    tar = parseFloat(tarjeta);
    otros = $('#otros_cotizacion2_'+numero_cliente).val();    
    otros = otros.replace(/,/g, "");
    ot = parseFloat(otros);

    total = d + t + tar + ot;
    tot = parseFloat((total).toFixed(2));
    
    $('#total_cotizacion2_'+numero_cliente).text('$'+tot.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));  
    
    console.log(idElement);

    console.log($('#tenencia_cotizacion2_'+numero_cliente).val());
    console.log(d);
    console.log(t);
    console.log(tar);
    console.log(ot);

    console.log(tot);

  });

  $(".cotizacion2").change(function(){

    //let total = parseInt($(this).val());
    let idElement = $(this).attr('id');
    let attrElement = idElement.split("_");
    let numero_cliente = attrElement[2];
    console.log(numero_cliente);

    let derechos = 0;
    let tenencia = 0;
    let tarjeta = 0;
    let otros = 0;
    let total = 0;

    derechos = $('#derechos_cotizacion2_'+numero_cliente).val();
    derechos = derechos.replace(/,/g, "");
    d = parseFloat(derechos);
    tenencia = $('#tenencia_cotizacion2_'+numero_cliente).val();
    tenencia = tenencia.replace(/,/g, "");
    t = parseFloat(tenencia);
    tarjeta = $('#tarjeta_cotizacion2_'+numero_cliente).val();
    tarjeta = tarjeta.replace(/,/g, "");
    tar = parseFloat(tarjeta);
    otros = $('#otros_cotizacion2_'+numero_cliente).val();    
    otros = otros.replace(/,/g, "");
    ot = parseFloat(otros);

    total = d + t + tar + ot;
    tot = parseFloat((total).toFixed(2));
    
    $('#total_cotizacion2_'+numero_cliente).text('$'+tot.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));  
    
    console.log(idElement);

    console.log($('#tenencia_cotizacion2_'+numero_cliente).val());
    console.log(d);
    console.log(t);
    console.log(tar);
    console.log(ot);

    console.log(tot);


  });

  $(".cotizacion3").change(function(){

    //let total = parseInt($(this).val());
    let idElement = $(this).attr('id');
    let attrElement = idElement.split("_");
    let numero_cliente = attrElement[2];
    console.log(numero_cliente);

    let derechos = 0;
    let tenencia = 0;
    let tarjeta = 0;
    let otros = 0;
    let total = 0;

    derechos = $('#derechos_cotizacion2_'+numero_cliente).val();
    derechos = derechos.replace(/,/g, "");
    d = parseFloat(derechos);
    tenencia = $('#tenencia_cotizacion2_'+numero_cliente).val();
    tenencia = tenencia.replace(/,/g, "");
    t = parseFloat(tenencia);
    tarjeta = $('#tarjeta_cotizacion2_'+numero_cliente).val();
    tarjeta = tarjeta.replace(/,/g, "");
    tar = parseFloat(tarjeta);
    otros = $('#otros_cotizacion2_'+numero_cliente).val();    
    otros = otros.replace(/,/g, "");
    ot = parseFloat(otros);

    total = d + t + tar + ot;
    tot = parseFloat((total).toFixed(2));
    
    $('#total_cotizacion2_'+numero_cliente).text('$'+tot.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));  
    
    console.log(idElement);

    console.log($('#tenencia_cotizacion2_'+numero_cliente).val());
    console.log(d);
    console.log(t);
    console.log(tar);
    console.log(ot);

    console.log(tot);
    
  });

  function blockinputs(cotizacion_n, numero_cliente){

    derechos = parseInt($('#derechos_'+cotizacion_n+'_'+numero_cliente).val());
    tenencia = parseFloat($('#tenencia_'+cotizacion_n+'_'+numero_cliente).val());
    tarjeta = parseInt($('#tarjeta_'+cotizacion_n+'_'+numero_cliente).val());
    otros = parseInt($('#otros_'+cotizacion_n+'_'+numero_cliente).val());

    (derechos > 0) ? 
      $('#derechos_'+cotizacion_n+'_'+numero_cliente).prop('disabled', true) 
      : $('#derechos_'+cotizacion_n+'_'+numero_cliente).prop('disabled', false) ;

    (tenencia > 0) ? 
      $('#tenencia_'+cotizacion_n+'_'+numero_cliente).prop('disabled', true) 
      : $('#tenencia_'+cotizacion_n+'_'+numero_cliente).prop('disabled', false) ;

    (tarjeta > 0) ? 
      $('#tarjeta_'+cotizacion_n+'_'+numero_cliente).prop('disabled', true) 
      : $('#tarjeta_'+cotizacion_n+'_'+numero_cliente).prop('disabled', false) ;

    (otros > 0) ? 
      $('#otros_'+cotizacion_n+'_'+numero_cliente).prop('disabled', true) 
      : $('#otros_'+cotizacion_n+'_'+numero_cliente).prop('disabled', false) ;
  }

  $(document).on('click','.popup',function(){
    let idElement = $(this).attr('id');
    let attrElement = idElement.split("_");
    let numero_cliente = attrElement[1];
    //console.log(attrElement);
    blockinputs("cotizacion1", numero_cliente);
    blockinputs("cotizacion2", numero_cliente);
    blockinputs("cotizacion3", numero_cliente);

  });


</script>

</div> <!-- este div cierra es el del principio -->

<?php include "footer.php"; ?>
