
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.js" ></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<!--
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap.min.js"></script> -->

<?php 
    $active="active"; 
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 


    function alert($msg,$val) {

        if($val==1){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','success');</script>";
        }else if($val==2){

            echo "<script type='text/javascript'>swal('Error Al Agregar','$msg','error');</script>";
        }else if($val==3){
             echo "<script type='text/javascript'>swal('Registro Movido','$msg','success');</script>";
        }else{
          echo "<script type='text/javascript'>swal('Error Al Mover','$msg','error');</script>";
        }


            
    }

    if(empty($_GET)){
        
    }else{

        if(!empty($_GET['success'])){

        switch ($_GET['success']) {
            case 'true':
                alert("Se han cargado exitosamente",1); 
                break;
            

            case 'false':
                alert("",2);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }

      if(!empty($_GET['move_success'])){

        switch ($_GET['move_success']) {
            case 'true':
                alert("A tabla En proceso",3); 
                break;
            

            case 'false':
                alert("",4);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }


    }

?>

<div class="content-wrapper" ><!-- Content Wrapper. Contains page content --
   <section class="content-header">
  <section class="content-header"> Content Header (Page header) -->
     <section class="content-header" hidden>
            <h1></h1>
            <ol class="breadcrumb">
                <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Importar Activaciones</li>
            </ol>
      </section>

        
<?php

//session_start();

//Modificar 

include "dbconect.php";
$datos = $con->query("select * from tbl_cotizador where no_cliente <> '' ");

$activos= $con->query("SELECT COUNT(*) Abiertos FROM db_sistema.tbl_staging;");

?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
<h1 style="padding-top: 25px;">&nbsp;&nbsp;COTIZACIONES</h1>

<div id="loader" style="display: none" ></div>


  <div style="padding-top: 50px; padding-bottom: 15px; padding-left:25px; font-size: 20px;">
    <form method="post" id="addproduct" action="import_4.php" enctype="multipart/form-data" role="form">
      <div <?php if($_SESSION["usuario"]=="Admin_JJ" || $usuario==0){echo "hidden";} ?> >
        <div class="row">
          <label class="custom-file-upload btn-info" style="padding-bottom: 10px; border-radius: 5px; border:solid 1px; ">
          <input type="file" name="name" class="custom-file-upload" id="carga_archivo" placeholder="Archivo (.xlsx)">
            <i class="fa fa-cloud-upload"></i>&nbsp; &nbsp;Cargar Archivo
          </label>
          <button type="submit" class="btn btn-success" style="font-size: 20px;" onclick="start_loader()" ><i class="fa fa-cloud-download" aria-hidden="true">
          </i><b>&nbsp; &nbsp;Importar Datos</b></button>
        </div>
      </div>
      <div class="row" id="div_file_name" style="display: none; width: 500px;" >
        <input type="text" id="name_file" style="width: 390px;" disabled>
      </div>
    </form>
  </div>

<?php if($datos->num_rows>0):?>
  
  <div class="container-fluid" >
  <div class="row" id="tabla">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
  <table border="1"id="myTable"class="table-bordered table-hover display compact" style="width:100%" >
  <thead>
        
          <th  style="text-align:center">Fecha</th>
          <th  style="text-align:center">N°cliente</th>
            <th  style="text-align:center">Tipo</th> 
            <th hidden style="text-align:center">Serie de la unidad</th> <!-- -->
            <th style="text-align:center">Marca</th><!-- -->
            <th hidden style="text-align:center">Entidad</th> <!-- -->
            <th hidden style="text-align:center">Valor de la unidad</th> <!-- -->
            <th  style="text-align:center">Ejecutivo</th>
            <th hidden style="text-align:center">Regional</th>
            <th hidden style="text-align:center">Agencia</th>
            <th <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> style="text-align:center">Factura</th>
            <th <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> style="text-align:center">Proceso de Cotizaci&oacute;n</th>
            <th <?php if($_SESSION["usuario"]!="Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaciones</th>

            <th hidden <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Estado 1</th> 
            <th hidden <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 1</th>
            <th hidden <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 1</th>
            <th hidden <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Aceptar</th>

            <th hidden <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Estado 2</th>
            <th hidden <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 2</th>
            <th hidden <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 2</th>
            <th hidden <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Aceptar</th>

            <th hidden <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Estado 3</th>
            <th hidden <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 3</th>
            <th hidden <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 3</th>
            <th hidden <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Aceptar</th>

            <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Comprobante</th>
            <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Guardar</th>

            <th <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> style="text-align:center">Rechazar</th>

             <th <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Actualizar</th>

  </thead>
  <tbody>
  <?php while($d= $datos->fetch_object()):?>
    <tr align="center">
    <td >
      <?php 
    echo '  

    <a data-toggle="modal" href="#Modal_datos_'.$d->no_cliente.'"><i class="fa fa-plus-circle  icon" aria-hidden="true"></i></a>

    <div class="modal fade" id="Modal_datos_'.$d->no_cliente.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                        </button>
                         <h3 class="modal-title" id="myModalLabel"><b>'.$d->nombre.'</b></h3>

                    </div>
                    <div class="modal-body" style="text-align:left;">
                     <ul>
                      <li>MODELO: '.$d->modelo.'</li>
                      <li>VERSI&Oacute;N: '.$d->version.'</li>
                      <li>AÑO: '.$d->anio.'</li>
                      <li>VALOR FACTURA: '.$d->precio.'</li>
                      <li>ESTADO ORIGEN: '.$d->edo_origen.'</li>
                      <li>PDV: '.$d->pdv.'</li>
                      <li>VENDEDOR: '.$d->vendedor.'</li>
                     </ul>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    ';?>
    <?php echo "&nbsp".$d->fecha_creacion; ?>
    </td>

    <td class="numero_cliente"><?php echo $d->no_cliente; ?></td>
    <td class="<?php echo "vin_".$d->No_CLIENTE; ?>"><?php echo $d->tipo; ?></td>
    <td hidden class="<?php echo "vin_".$d->no_cliente; ?>"><?php echo $d->SERIE_DE_UNIDAD; ?></td>
    <td class="<?php echo "nombre_".$d->no_cliente; ?>"><?php echo $d->marca; ?></td>
    <td hidden class="<?php echo "entidad_".$d->no_cliente; ?>"><?php echo $d->ENTIDAD_SOLICITADA; ?></td>
    <td hidden class="<?php echo "entidad_".$d->no_cliente; ?>"><?php echo $d->VALOR_DE_LA_UNIDAD; ?></td>
    <td class="<?php echo "entidad_".$d->no_cliente; ?>"><?php echo $d->ejecutivo; ?></td>
    <td hidden class="<?php echo "entidad_".$d->no_cliente; ?>"><?php echo $d->REGIONAL; ?></td>
    <td hidden class="<?php echo "entidad_".$d->no_cliente; ?>"><?php echo $d->AGENCIA; ?></td>
     
   
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> >
        <label class=" custom-file-upload">
        <input type="file" class="factura" accept="application/pdf" id="Factura_<?php echo $d->no_cliente; ?>" >
        <i class="fa fa-cloud-upload"></i>
        </label>
     </td>
    <td class="estado" <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> >
      <?php
          echo '

        <button class="btn-default" id="_'.$d->no_cliente.'" 
        data-toggle="modal" data-target="#myModal_'.$d->no_cliente.'" style="width: 80px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </button>

        <div class="modal fade" id="myModal_'.$d->no_cliente.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="vertical-alignment-helper">
              <div class="modal-dialog vertical-align-center">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                          </button>
                           <h4 class="modal-title" id="myModalLabel"><b>Iniciar Proceso de cotizaci&oacute;n: '.$d->nombre.'</b></h4>
                      </div>
                      <div class="container">
                          <label class="modal-title" id="label_'.$d->no_cliente.'"></label>
                      </div>
                      <div class="modal-body">
                        <table border="1" class="table-bordered table-hover" style="width:100%">
                          <thead>
                            <th  style="text-align:center; background-color:#00c0ef;">Cotizaci&oacute;n</th>
                            <th style="text-align:center; background-color: #00c0ef;">Estado</th>
                            
                            <th style="text-align:center; background-color: #00c0ef;">Derechos</th>
                            <th style="text-align:center; background-color: #00c0ef;">Tenencia</th>
                            <th style="text-align:center; background-color: #00c0ef;">Tarjeta</th>
                            <th style="text-align:center; background-color: #00c0ef;">Otro costo</th>
                            <th style="text-align:center; background-color: #00c0ef;">Total</th>
                            <th style="text-align:center;"></th>
                          </thead>

                          <tbody>
                            <tr style="text-align:center">
                            <td>1</td>
                            <td>'.$d->edo_cotizacion_1.'
                            </td>';
                              switch ($d->edo_cotizacion_1) {
                                  
                                  case 'MEX':
                                      $derechos=796;
                                      $otro_costo=660;
                                      $tarjeta=0;
                                      $tenencia=0;
                                      $total=$derechos+$otro_costo+$tarjeta+$tenencia;
                                  
                                  break;

                                  case 'MOR':
                                      $derechos=885;
                                      $otro_costo=0;
                                      $tarjeta=0;
                                      $tenencia=0;
                                      $total=$derechos+$otro_costo+$tarjeta+$tenencia;
                                  
                                  break;

                                  case 'CMX':
                                      $derechos=741.5;
                                      $otro_costo=0;
                                      $tarjeta=0;
                                      $tenencia=0;
                                      $total=$derechos+$otro_costo+$tarjeta+$tenencia;
                                  
                                  break;
                                  
                                  default:
                                      $derechos="";
                                      $otro_costo="";
                                      $tarjeta="";
                                      $tenencia="";
                                      $total="";
                                    break;
                                }


                            echo'
                            <td>'.$derechos.'</td>
                            <td>'.$tenencia.'</td>
                            <td>'.$tarjeta.'</td>
                            <td>'.$otro_costo.'</td>
                            <td>'.$total.'</td>
                            <td>
                              <button class="cotizar btn btn-success"';
                                if($d->edo_cotizacion_1 == null ){echo "disabled ";}
                                  echo 'id="cotizar1_'.$d->no_cliente.'" >
                                  <i class="fa fa-check" aria-hidden="true"></i>
                              </button>
                            </td>
                            </tr>
                            <tr style="text-align:center">
                            <td>2</td>
                            <td>'.$d->edo_cotizacion_2.'
                            </td>
                            ';
                              switch ($d->edo_cotizacion_2) {
                               
                                case 'MEX':
                                      $derechos=796;
                                      $otro_costo=660;
                                      $tarjeta=0;
                                      $tenencia=0;
                                      $total=$derechos+$otro_costo+$tarjeta+$tenencia;
                                  
                                  break;

                                  case 'MOR':
                                      $derechos=885;
                                      $otro_costo=0;
                                      $tarjeta=0;
                                      $tenencia=0;
                                      $total=$derechos+$otro_costo+$tarjeta+$tenencia;
                                  
                                  break;

                                  case 'CMX':
                                      $derechos=741.5;
                                      $otro_costo=0;
                                      $tarjeta=0;
                                      $tenencia=0;
                                      $total=$derechos+$otro_costo+$tarjeta+$tenencia;
                                  
                                  break;
                                  
                                  default:
                                      $derechos="";
                                      $otro_costo="";
                                      $tarjeta="";
                                      $tenencia="";
                                      $total="";
                                    break;
                                }

                            echo '
                            <td>'.$derechos.'</td>
                            <td>'.$tenencia.'</td>
                            <td>'.$tarjeta.'</td>
                            <td>'.$otro_costo.'</td>
                            <td>'.$total.'</td>
                            <td>
                              <button class="cotizar btn btn-success"';
                                if($d->edo_cotizacion_2 == null ){echo "disabled ";}
                                  echo 'id="cotizar2_'.$d->no_cliente.'" >
                                  <i class="fa fa-check" aria-hidden="true"></i>
                              </button>
                            </tr>
                            <tr style="text-align:center">
                            <td>3</td>
                            <td>'.$d->edo_cotizacion_3.'
                            </td>';
                              switch ($d->edo_cotizacion_3) {
                                
                                case 'MEX':
                                      $derechos=796;
                                      $otro_costo=660;
                                      $tarjeta=0;
                                      $tenencia=0;
                                      $total=$derechos+$otro_costo+$tarjeta+$tenencia;
                                  
                                  break;

                                  case 'MOR':
                                      $derechos=885;
                                      $otro_costo=0;
                                      $tarjeta=0;
                                      $tenencia=0;
                                      $total=$derechos+$otro_costo+$tarjeta+$tenencia;
                                  
                                  break;

                                  case 'CMX':
                                      $derechos=741.5;
                                      $otro_costo=0;
                                      $tarjeta=0;
                                      $tenencia=0;
                                      $total=$derechos+$otro_costo+$tarjeta+$tenencia;
                                  
                                  break;
                                  
                                  default:
                                      $derechos="";
                                      $otro_costo="";
                                      $tarjeta="";
                                      $tenencia="";
                                      $total="";
                                    break;
                                }
                            echo'
                            <td>'.$derechos.'</td>
                            <td>'.$tenencia.'</td>
                            <td>'.$tarjeta.'</td>
                            <td>'.$otro_costo.'</td>
                            <td>'.$total.'</td>
                            <td>
                              <button class="cotizar btn btn-success"';
                                if($d->edo_cotizacion_3 == null ){echo "disabled ";}
                                  echo 'id="cotizar3_'.$d->no_cliente.'" >
                                  <i class="fa fa-check" aria-hidden="true"></i>
                              </button></td>
                            </tr>
                          </tbody>

                        </table>

                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                  </div>
              </div>
          </div>
        </div>

      ';

    ?>
    <td <?php if($_SESSION["usuario"]!="Admin_JJ"){echo "hidden";} ?> >
      <?php
          echo '

        <button class="btn-default" id="_'.$d->No_CLIENTE.'" 
        data-toggle="modal" data-target="#myModal_admin'.$d->No_CLIENTE.'" style="width: 80px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </button>

        <div class="modal fade" id="myModal_admin'.$d->No_CLIENTE.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="vertical-alignment-helper">
              <div class="modal-dialog vertical-align-center">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                          </button>
                           <h4 class="modal-title" id="myModalLabel"><b>Iniciar Proceso</b></h4>

                      </div>
                      <div class="modal-body">
                        <table id="table_'.$d->No_CLIENTE.'" class=""style="width:100%">
                          <thead>
                            <th style="text-align:center">Cotizaci&oacute;n</th>
                            <th style="text-align:center">Estado</th>
                            <th style="text-align:center">Valor</th>
                            <th style="text-align:center"></th>
                          </thead>

                          <tbody>
                            <tr style="text-align:center">
                            <td>1</td>
                            <td class="estado_cell" >'.$d->estado1.'</td>
                            
                            <td><input type="text" id="valor1_'.$d->No_CLIENTE.'" class="cotizacion"';
                             if($d->estado1 == null){echo "value='0' disabled ";}else {echo "value=".$d->cotizacion1;} echo ' > </td>

                            <td><button class="cotizar btn btn-success"';
                            if($d->estado1 == null){echo "disabled ";}
                            echo 'id="cotizar1_'.$d->No_CLIENTE.'" >
                            <i class="fa fa-check" aria-hidden="true"></i></button>
                            </td>

                            </tr>
                            <tr style="text-align:center">
                            <td>2</td>
                            <td class="estado_cell">'.$d->estado2.'</td>

                            <td><input type="text" id="valor2_'.$d->No_CLIENTE.'" class="cotizacion"';
                             if($d->estado2 == null){echo "value='0' disabled ";}else {echo "value=".$d->cotizacion2;} echo ' > </td>
                            
                            <td><button class="cotizar btn btn-success"';
                            if($d->estado2 == null ){echo "disabled ";}
                            echo 'id="cotizar2_'.$d->No_CLIENTE.'" >
                            <i class="fa fa-check" aria-hidden="true"></i></button>
                            </td>

                            </tr>
                            <tr style="text-align:center">
                            <td>3</td>
                            <td class="estado_cell">'.$d->estado3.'</td>

                            <td><input type="text" id="valor3_'.$d->No_CLIENTE.'" class="cotizacion"';
                             if($d->estado3 == null){echo "value='0' disabled ";}else {echo "value=".$d->cotizacion3;} echo ' > </td>
                            
                            <td><button class="cotizar btn btn-success"';
                            if($d->estado3 == null ){echo "disabled ";}
                            echo 'id="cotizar3_'.$d->No_CLIENTE.'" >
                            <i class="fa fa-check" aria-hidden="true"></i></button>
                            </td>
                            </tr>
                          </tbody>

                        </table>

                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      ';

    ?>

    </td>
   
    <td hidden <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>><?php echo $d->estado1; ?></td>
    <td hidden
      <?php 
        if($_SESSION["usuario"]!="Admin_JJ"){echo "hidden";} 
      ?> 
    >
      <input type="text" id="<?php echo "valor1_".$d->No_CLIENTE;?>" class="cotizacion" <?php if($d->estado1 == null){echo "value='0' disabled";} else {echo "value=".$d->cotizacion1;} ?>>
    </td>
    

    <td hidden <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><?php echo $d->cotizacion1; ?></td>
    <td hidden <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><button class="cotizar btn btn-success"
    <?php if($d->estado1 == null || $d->cotizacion1 == 0){echo "disabled";}?> 
    id="<?php echo "cotizar1_".$d->No_CLIENTE; ?>" ><i class="fa fa-check" aria-hidden="true"></i></button></td>

    <td hidden <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>><?php echo $d->estado2; ?></td>
    <td hidden
      <?php 
        if($_SESSION["usuario"]!="Admin_JJ"){echo "hidden";} 
      ?> 
    >
      <input type="text" id="<?php echo "valor2_".$d->No_CLIENTE; ?>" class="cotizacion" <?php if($d->estado2 == null){echo "value='0' disabled";}else {echo "value=".$d->cotizacion2;} ?>>
    </td>
    <td hidden <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><?php echo $d->cotizacion2; ?></td>
    <td hidden <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><button class="cotizar btn btn-success"
    <?php if($d->estado2 == null || $d->cotizacion2 == 0){echo "disabled";} ?> id="<?php echo "cotizar2_".$d->No_CLIENTE; ?>"  ><i class="fa fa-check" aria-hidden="true"></i></button></td>


    <td hidden <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> ><?php echo $d->estado3; ?></td>
    <td hidden
      <?php 
        if($_SESSION["usuario"]!="Admin_JJ"){echo "hidden";} 
      ?> 
    >
      <input type="text" id="<?php echo "valor3_".$d->No_CLIENTE;?>" class="cotizacion" <?php if($d->estado3 == null){echo "value='0' disabled";}else {echo "value=".$d->cotizacion3;} ?>>
    </td>
    <td hidden <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><?php echo $d->cotizacion3; ?></td>
    <td hidden <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><button class="cotizar btn btn-success"
    <?php if($d->estado3 == null || $d->cotizacion3 == 0){echo "disabled";} ?> id="<?php echo "cotizar3_".$d->No_CLIENTE; ?>"  ><i class="fa fa-check" aria-hidden="true"></i></button></td>

    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> >
          <label class=" custom-file-upload">
          <input type="file" id="<?php echo "Comprobante_".$d->no_cliente; ?>" accept="application/pdf" disabled>
          <i class="fa fa-cloud-upload"></i>
          </label>
            <!--<div class="new_Btn"><i class="fa fa-cloud-upload"></i>Factura</div><br>
            <input  type='file' style="display:none"/><br> -->
     </td>
     <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>  >
      <?php
          echo '

     <form id="form_'.$d->no_cliente.'" action="en_proceso.php" method="POST" style="display:block; margin:auto;">
      <input type="text" name="numero_cliente" value="'.$d->no_cliente.'" hidden>
      <input id="num_button_'.$d->no_cliente.'" name="opcion" type="text" hidden>
      
      </form>
      <button class="btn btn-primary" type="button" id="Guardar_'.$d->no_cliente.'" onclick="mi_funcion('.$d->no_cliente.')" disabled 
      ><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
      ';

    ?>
      </td>



    <td <?php  if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>>
      <?php
        echo '
      <form action="rechazados2.php" method="post" style="display:block; margin:auto;">
      <input type="text" name="numero_cliente" value="'.$d->no_cliente.'" hidden >
      <button class="btn btn-danger" id="Rechazar_"'.$d->no_cliente.'"  
      ><i class="fa fa-times" aria-hidden="true"></i></button>
      </form>     
        ';  
      ?>         
    </td>

     <td <?php  if($_SESSION["usuario"]!="Admin_JJ"){echo "hidden";} ?> >
      <?php  echo '
            <button class="btn-click-action btn btn-success" id="button_'.$d->no_cliente.'" name="button_'.$d->no_cliente.'"
            ><i class="fa fa-refresh" ></i></button>

            ';  
      ?>
    </td>

  <?php endwhile; ?>
  </tbody>
</table>
</div>
</div>
</div>
</div>
<?php else:?>
  <h3 style="padding-left:15px;">NO SE HAN CARGADO DATOS</h3>
<?php endif; ?>
  
</body>



<script>



$(".btn-click-action").click(function() {

  var name= $(this).attr('name');
       
  var identificador = name.split("_");
   
  var valor1= "valor1_"+identificador[1];
  var valor2= "valor2_"+identificador[1];
  var valor3= "valor3_"+identificador[1];
  
  var nombre = $('.nombre_'+identificador[1]).val();
  var entidad = $('.entidad_'+identificador[1]).val();
  var vin = $('.vin_'+identificador[1]).val();

  console.log(identificador);

  console.log($("#"+valor1).val());
  console.log($("#"+valor2).val());
  console.log($("#"+valor3).val());
  
  if($("#"+valor1).val() == "" || $("#"+valor2).val() == "" || $("#"+valor3).val() == ""){
  
  
    swal("Campos Vacios", "Llene los campos de cotizacion", "info");
  
    //alert("derechos y honorarios no pueden ser vacios");
  
  }else{
  
    
    $.ajax({
        url: "action/updatelayout.php",
        type: 'POST',
       
        async : true,
        data:{ 
          'valor1' : $("#"+valor1).val(),
          'valor2' : $("#"+valor2).val(),
          'valor3' : $("#"+valor3).val(),
          'nombre' : nombre,  
          'entidad' : entidad,
          'vin' : vin,
          'id' : identificador[1]
         } ,
        success: function(resp) {

      
      //alert("Datos Actualizados"); 
      swal("Datos Actualizados", "", "success");
      
      
      //window.location.reload(true);

        },
        error: function(request,err){
      console.log("error")
      console.log(err)
      

        }
    });
    
  }

});


  function start_loader(){

    document.getElementById("loader").style.display = "block";
  }

  $(".factura").change(function(){

        var identificador=$(this).attr('id');
        var id = identificador.split("_");
        var archivos = new FormData();
        var factura = $('#Factura_'+id[1])[0].files[0];
        //var comprobante = $('#Comprobante_'+id)[0].files[0];
        archivos.append('factura',factura);
        archivos.append('id',id[1]);

        swal({
          title: factura.name,
          text: "¿Cargar archivo?",
          icon: "info",
          buttons: ["Cancelar",true],
          dangerMode: true,
        })
        .then((value) => {
          if (value) {

            $.ajax({
                url: 'action/uploads_factura.php',
                type: 'post',
                data: archivos,
                contentType: false,
                processData: false,
                success: function(response){
                    if(response > 0){
                        swal("Archivo cargado exitosamente", {
                          icon: "success",
                        });
                    }else{
                        alert('Error al cargar el archivo','','error');
                    }
                },
            });
            
          } else {
            $('#Factura_'+id[1]).val("");
          }
        });
  });

  function view(valor){
            console.log(valor.name);
        }


  function mi_funcion(id){

     console.log("Identificador: "+id); 

    //alert("Boton Donde se activo "+id);

        if($('#Comprobante_'+id).val() != ""){

          var archivos = new FormData();
          //var factura = $('#Factura_'+id)[0].files[0];
          var comprobante = $('#Comprobante_'+id)[0].files[0];
          
          // archivos.append('factura',factura);
          archivos.append('comprobante',comprobante);
          archivos.append('id',id);

          //console.log(fd['file1']);

          document.getElementById("loader").style.display = "block";

          $.ajax({
              url: 'action/uploads_files.php',
              type: 'post',
              data: archivos,
              contentType: false,
              processData: false,
              success: function(response){
                  if(response > 0){

                       document.getElementById("form_"+id).submit();                      

                      //$("#img").attr("src",response); 
                      //$(".preview img").show(); // Display image element
                      //setTimeout(function(){ alert("Hello"); }, 3000);
                      //alert("Archivos Subidos Correctamente");
                      //swal( {closeOnClickOutside: false, timer: 4000},"Archivos Cargados","","success");
                  }else{
                      alert('file not uploaded');
                  }
              },
          });

        }else{
          swal("No se ha cargado Comprobante","Agrege el documento","info");
        }
  }


 $("#carga_archivo").change(function(){

    var path= $(this).val();
    
    var name  = path.split('\\');

    $("#name_file").val("Archivo: "+name[2]);

    console.log(name);

     document.getElementById("div_file_name").style.display = "block";

 });


$(".cotizar").click(function(){

      var name= $(this).attr('id');
       
      var identificador = name.split("_");

      console.log("Se dio aceptar: "+identificador);

      $("#num_button_"+identificador[1]).val(identificador[0]);


      switch(identificador[0]){

        case "cotizar1" :
           document.getElementById("cotizar2_"+identificador[1]).disabled = true;
           document.getElementById("cotizar3_"+identificador[1]).disabled = true;
           var cotizacion= 1; 
          break;

        case "cotizar2" :
          document.getElementById("cotizar1_"+identificador[1]).disabled = true;
          document.getElementById("cotizar3_"+identificador[1]).disabled = true;
          var cotizacion= 2; 
          break;

        case "cotizar3" :
          document.getElementById("cotizar1_"+identificador[1]).disabled = true;
          document.getElementById("cotizar2_"+identificador[1]).disabled = true;
          var cotizacion= 3; 
          break;

      }

      $("#label_"+identificador[1]).text("Se ha aceptado la cotización: "+cotizacion);
      $("#label_"+identificador[1]).css("color", "green");

      document.getElementById("Factura_"+identificador[1]).disabled = false;
      document.getElementById("Comprobante_"+identificador[1]).disabled = false;
      document.getElementById("Guardar_"+identificador[1]).disabled = false;
        

      /*
        desabilitar los demas botones

      */



      $.ajax({
            url: 'action/set_autorizacion.php',
            type: 'post',
            data: {'name': identificador[0],
                  'id' : identificador[1]
                  },
           
            success: function(response){
                
            },
        });

    });


$(document).ready( function () {

   
        
});

  /*
  {"targets": [ 1 ],
                "visible": false,
                "searchable": false
              }



  */


      
      $("#myTable").DataTable({
      
        //autoWidth: true,
       //ScrollX: true,
       
       /*

       "fnInitComplete": function(oSettings) {
                        $( window ).resize();
                    },

       "fnDrawCallback": function(oSettings) {
            $( window ).trigger('resize');
          }
          */



       //scrollCollapse: true,    
      /* "columnDefs": [
      { className: "my_class", "targets": [1,2,3,4,5,6,7,8,9,12] },
         { responsivePriority: -1, targets: -3 },
        { responsivePriority: 2, targets: 2 },
        { responsivePriority: 3, targets: 1 },
        { responsivePriority: -1,"width" :"1%", "targets": 0},
        { responsivePriority: -1,"width" :"1%", "targets": 1},

        { responsivePriority: -9, "width" :"1%", "targets": 10},
        { responsivePriority: -9, "width" :"1%", "targets": 11},
        //{ responsivePriority: -9, "width" :"1%", "targets": 12},
        
        { responsivePriority: -1, "width" :"1%", "targets": 13},
        { responsivePriority: -1, "width" :"1%", "targets": 14},
        { responsivePriority: -1, "width" :"1%", "targets": 15},
        { responsivePriority: -1, "width" :"1%", "targets": 16},
        { responsivePriority: -1, "width" :"1%", "targets": 17},
        
        { responsivePriority: -1, "width" :"1%", "targets": 10},
        { responsivePriority: -1, "width" :"1%", "targets": 11},
        { responsivePriority: -1, "width" :"1%", "targets": 24},
        { responsivePriority: -1, "width" :"1%", "targets": 25},
        { responsivePriority: -1, "width" :"1%", "targets": 26},
        { responsivePriority: -10, "width" :"1%", "targets": 27},
        
        //{ targets: -1 }
      ],
      */
      /*
      responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return data[4]+"<hr style='border:1px solid'>";
                    }
                } ),
                renderer: function ( api, rowIdx, columns ) {
                var data = $.map( columns, function ( col, i ) {

                        if(col.className=="my_class"){
                          
                        return '<h3>'+col.title+":"+col.data+'</h3>';

                      }else{
                        return '';
                      }


                } ).join('');
 
                return data ?
                    data :
                    false;
            }
            }
        }*/

      /*

        responsive: {
        details: {
            renderer: function ( api, rowIdx, columns ) {
                var data = $.map( columns, function ( col, i ) {

                        if(col.className=="my_class" || col.hidden){
                           return '<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
                            '<td>'+col.title+':'+'</td> '+
                            '<td>'+col.data+'</td>'+
                        '</tr>'
                      }else{
                        return '';
                      }


                } ).join('');
 
                return data ?
                    $('<table/>').append( data ) :
                    false;
            }
        }
    }
    */



    
    });


    $("#myTable tbody tr").each(function() {

         var numero_cliente= $(this).find("td.numero_cliente").text();


          $(this).find("td.estado").each(function() {

            $("#table_"+numero_cliente+" tbody tr").each(function() {
                var valor= $(this).find("td.estado_cell").text();

                console.log(valor);
              });
          });

          //console.log(estado);


            //console.log($(this).find("td.fecha_cartera").text());
            //console.log("Hola");

            /*

            var fecha_pago=$(this).find("td.fecha_cartera").text();

            for (var i = 0; i < pagos.length ; i++) {

                var fecha=pagos[i]["fecha_pago"];
                var data=fecha.split('-');

                var newfecha=data[2]+'-'+data[1]+'-'+data[0];

                console.log(newfecha);


                if(fecha_pago==newfecha){
                 $(this).find("td.celda_estatus").text("Pagado");
                }else{

                }
            }*/

        });



      
</script>

</html>

</div> <!-- este div cierra es el del principio -->

<?php include "footer.php"; ?>

<!--

 

  </form>

  -->