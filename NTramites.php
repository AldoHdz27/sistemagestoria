<link rel="icon" href="images/JJ.ico">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js" ></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



<?php 
    $active="active"; 
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 


     function alert($msg,$val) {

        if($val==1){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','success');</script>";
        }else if($val==2){

            echo "<script type='text/javascript'>swal('Error Al Agregar','$msg','error');</script>";
        }else if($val==3){
             echo "<script type='text/javascript'>swal('Registro Movido','$msg','success');</script>";
        }else{
          echo "<script type='text/javascript'>swal('Error Al Mover','$msg','error');</script>";
        }


            
    }

    if(empty($_GET)){
        
    }else{

        if(!empty($_GET['success'])){

        switch ($_GET['success']) {
            case 'true':
                alert("Se han cargado exitosamente",1); 
                break;
            

            case 'false':
                alert("",2);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }

      if(!empty($_GET['move2_success'])){

        switch ($_GET['move2_success']) {
            case 'true':
                alert("A Tabla Rechazados",3); 
                break;
            

            case 'false':
                alert("",4);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }
    }
?>

<div class="content-wrapper"><!-- Content Wrapper. Contains page content --
   <section class="content-header">
  <section class="content-header"> Content Header (Page header) -->
     
        
<?php

include_once "config/config.php";
date_default_timezone_set ('America/Mexico_City');
$datos = $con->query("SELECT * FROM v_tramites");

?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
<h1>&nbsp; &nbsp; &nbsp; TR&Aacute;MITES</h1>

<?php if($datos->num_rows>0):?>
  
  <div class="container-fluid" >
  <div class="row" id="tabla">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
  <table border="1"id="myTable"class="table table-bordered table-hover nowrap" style="width:100%" >
  <thead>
    <th style="text-align:center">FECHA</th>
    <th style="text-align:center">NO CLIENTE</th>
    <th style="text-align:center">NOMBRE</th>

    <th style="text-align:center">VALOR FACTURA</th>
    <th style="text-align:center">EDO ORIGEN</th>
    <th style="text-align:center">PDV</th>

    <th style="text-align:center" <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> >DOCUMENTOS</th>

    <th style="text-align:center" <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>GESTOR</th>
    <th style="text-align:center" <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?>>GESTOR</th>
    
    <th style="text-align:center">EDO PLACAS</th>
    <th style="text-align:center">DIAS</th> 
    <th style="text-align:center" <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>ESTATUS</th> 
    <th style="text-align:center" <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?>>ESTATUS</th> 
    
    <th style="text-align:center" <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>COMENTARIOS </th>
    <th style="text-align:center" <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>ACEPTAR</th>

  </thead>
  <tbody>
  <?php while($d= $datos->fetch_object()):?>
    
    <tr align="center">
      <td ><?php 
        echo 
        '<a data-toggle="modal" href="#Modal_datos_'.$d->NO_CLIENTE.'"><i class="fa fa-plus-circle  icon" aria-hidden="true"></i></a>
        <div class="modal fade" id="Modal_datos_'.$d->NO_CLIENTE.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                            </button>
                             <h3 class="modal-title" id="myModalLabel"><b>'.$d->NOMBRE.'</b></h3>

                        </div>
                        <div class="modal-body" style="text-align:left;">
                         <ul>
                          <li>FOLIO: '.$d->FOLIO.'</li>
                          <li>TIPO: '.$d->TIPO.'</li>
                          <li>MARCA: '.$d->MARCA.'</li>
                          <li>MODELO: '.$d->MODELO.'</li>
                          <li>VERSION: '.$d->VERSION.'</li>
                          <li>AÑO: '.$d->ANIO.'</li>
                         </ul>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
      ?>
      <?php echo "&nbsp".$d->FECHA; ?></td>
      <td ><?php echo $d->NO_CLIENTE; ?></td>
      <td ><?php echo $d->NOMBRE; ?></td>

      <td ><?php echo '$'.number_format($d->VALOR_FACTURA,2); ?></td>
      <td ><?php echo $d->EDO_ORIGEN; ?></td>
      <td ><?php echo $d->PDV; ?></td>

      <td <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>

        <?php
         echo 
          '<button class="popup-documentos btn btn-primary" id="'.$d->ID.'_'.$d->NO_CLIENTE.'" 
            data-toggle="modal" data-target="#Modal-Documentos">
            <i class="fa fa-file-text-o" aria-hidden="true"></i>
          </button>';
        ?>
      </td>
      <td <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>

        <?php 
          $querygestores = "SELECT G.CODIGO, G.NOMBRE FROM tbl_gestores G
            WHERE G.ACTIVO = 1 AND G.ISO = '$d->EDO_PLACAS'";

          $result = mysqli_query($con, $querygestores);
          $data_gestores = mysqli_fetch_all($result, MYSQLI_ASSOC);
        ?>

        <select class='gestores' id="gestores_<?php echo $d->ID; ?>">
          <option selected disabled>Seleccione un gestor</option>
          <?php  

            $selected = '';
            $codigo = '';

            foreach ($data_gestores as $dg) {


              if($dg['NOMBRE'] == $d->GESTOR){
                $selected = ' selected';
                $codigo = $dg['CODIGO'];
              }

              echo "<option value=".$dg['CODIGO'].$selected.">".$dg['NOMBRE']."</option>";
            }
          ?>
          
        </select>
      </td>
      
      <td <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?>><?php echo
            $d->GESTOR; 
        ?>
      </td>  
      <td><?php echo $d->EDO_PLACAS; ?></td>
      <td><?php echo $d->DIAS; ?></td>
      <td <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>><?php 

        $desabilitar1 = '';
        $desabilitar2 = '';
        $desabilitar3 = '';
       


        switch ($d->ESTATUS) {
          case 'ASIGNADO':
            $desabilitar1 = ' disabled';
            $desabilitar2 = '';
            $desabilitar3 = '';
            
            break;

          case 'INCOMPLETO':
            $desabilitar1 = ' disabled';
            $desabilitar2 = ' disabled';
            $desabilitar3 = '';
          break;
          
          case 'COMPLETO':
            $desabilitar1 = ' disabled';
            $desabilitar2 = ' disabled';
            $desabilitar3 = ' disabled';
          break;
          
        
        }





      echo'
        <select name="estatus_'.$d->NO_CLIENTE.'" id="estatus_'.$d->ID.'" class="estatus">
          <option value=30' ;if($d->ESTATUS == 'PENDIENTE'){ echo ' selected ';} echo ' disabled >'.'PENDIENTE'.'</option>
          <option value=35' ;if($d->ESTATUS == 'ASIGNADO'){ echo ' selected '; } echo $desabilitar1.' >'.'ASIGNADO'.'</option>
          <option value=33' ;if($d->ESTATUS == 'INCOMPLETO') {echo ' selected ';} echo $desabilitar2.' >'.'INCOMPLETO'.'</option>
          <option value=36' ;if($d->ESTATUS == 'COMPLETO') {echo ' selected '; } echo $desabilitar3.' >'.'COMPLETO'.'</option>
          <option value=47';if($d->ESTATUS == 'ENTREGADO') {echo ' selected ';} echo ' >'.'ENTREGADO'.'</option>
        </select>';  
        ?>
      </td>
      <td <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?>><?php echo
            $d->ESTATUS
        ; 
        ?>
      </td>
      <td <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>><?php echo'
        <button data-toggle="modal" href="#Modal_comentarios_'.$d->NO_CLIENTE.'"><i class="fa fa-plus-circle  icon" aria-hidden="true"></i></button>
        <div class="modal fade" id="Modal_comentarios_'.$d->NO_CLIENTE.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                            <h4>COMENTARIOS </h4>
                        </div>
                        <div class="modal-body" style="text-align:left;">
                        <textarea class="comentarios" id="textarea_'.$d->ID.'" style="width:100%;"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
        ?>
      </td>
      <td <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>
        <?php
        echo '
          <form action="changeStatus_tramites.php" method="POST" style="margin-top:15px;">
        <input type="text" name="ID" value="'.$d->ID.'" hidden >
        <input type="text" id="value_estatus_'.$d->ID.'" name="value_estatus" value="'.$d->ESTATUS.'"  hidden >
        <input type="text" id="comentario_'.$d->ID.'" name="comentario" hidden >
        <input type="text" id="codigogestor_'.$d->ID.'" name="codigo_gestor" value = "'.$codigo.'" hidden>
        <button class="cotizar btn btn-primary btn-success" id="Guardar_'.$d->ID.'" 
        ><i class="fa fa-save" aria-hidden="true"></i></button>
          </form>
        ';
        ?>
      </td>
    </tr>



  <?php endwhile; ?>
  </tbody>
</table>
</div>
</div>
</div>
<?php else:?>
  <h3>No hay Datos</h3>
<?php endif; ?>

 <div class="modal fade modal-xl" id="Modal-Documentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content" style=" min-width:1000px !important;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"><b>Bitacora</b></h4>
                </div>
                <div class="container text-left" >
                    <label class="modal-title" id="label_bitacora"></label>
                </div>
                <div class="modal-body" style="overflow-y: auto; height: 150px;">
                    <table border="1" id="bitacora" style="border-collapse: collapse; width: 100%;">
                        <thead>
                            <tr>
                            <th style="text-align:center; background-color:#00c0ef; position: sticky; top: 0;">FECHA</th>
                            <th style="text-align:center; background-color:#00c0ef; position: sticky; top: 0;">DOCUMENTO</th>
                            <th style="text-align:center; background-color:#00c0ef; position: sticky; top: 0;">Descargar</th>

                            </tr>
                        </thead>
                        <tbody id="documentos">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<form id="formDescargarDocumento" method="post" action="downloadS3.php" hidden>
  <input id="keyvalue" type="text" name="key">
</form>



</body>



<script>




$(document).ready( function () {

   $("#myTable").DataTable({
      
       "scrollX": true
    
    });
      
   
      
});

$(document).on('click', '.descargarDocumento', function(){

  $('#keyvalue').val(this.id);

  document.getElementById("formDescargarDocumento").submit();          

});


$(document).on('click', '.popup-documentos', function(){

  let identificador= this.id;
  let values = identificador.split("_");
  let id = values[0];
  let no_cliente = values[0];
  let respuesta_documentos = document.querySelector('#documentos');
  respuesta_documentos.innerHTML = '';

      $.ajax({
        url:'action/getDocumentos.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
            'id' : id
        },
        success: function(resp) {
          for(let i= 0; i<resp.length; i++){
            respuesta_documentos.innerHTML += `
            <tr align="center">
              <td>${resp[i]['FECHA']}</td>
              <td>${resp[i]['DOCUMENTO']}</td>
              <td><button class="descargarDocumento btn-default" id="${resp[i]['DOCUMENTO']}">Descargar</button></td>
            </tr>`;
          }
        },
        error: function(request, err){
            console.log(err);
        }
    });

});



$( ".comentarios" ).change(function() {


  var identificador= this.id;
  var values = identificador.split("_");
  
  var id = values[1];

  var estatus =$(this).val();

  console.log($('#textarea_'+id).val()); 

  $('#comentario_'+id).val($('#'+identificador).val());

});

$( ".gestores" ).change(function() {


  var identificador= this.id;
  var values = identificador.split("_");

  var id = values[1];
  
  console.log(id);

  $('#codigogestor_'+id).val($('#'+identificador).val());

});




      
$( ".estatus" ).change(function() {


  var identificador= this.id;
  var values = identificador.split("_");
  
  var id = values[1];

  var estatus =$(this).val();

   console.log($('#value_estatus_'+id).val()); 


  $('#value_estatus_'+id).val(estatus);

  console.log(estatus);
  console.log(id);
  console.log($('#value_estatus_'+id).val()); 
 
});
      
</script>

</html>

</div> <!-- este div cierra es el del principio -->


<?php include "footer.php"; ?>
